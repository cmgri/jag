const mysql = require('mysql');
const fAuthor = require('./FunctionsAuthor');
const fCollection = require('./FunctionsCollection');
const fArtwork = require('./FunctionsArtwork');
const fAdmin = require('./FunctionsAdmin');
const fFiles = require('./FunctionsFiles');


module.exports = class DAO {

    constructor() {
        this.con = "";
    }

    connectdb() {

        if (this.con.state === "connected") {

            console.log("Database already status: " + this.con.state);

        } else {
            this.con = mysql.createPool({
                connectionLimit: 10,
                host: "eu-mm-auto-dub-02-a.cleardb.net",
                user: "b022406193c511",
                password: "7a988a20",
                database: "heroku_90eebc336b1b705"
            });
        
        }
    }

    checkActiveCollection(){

        this.connectdb()

        var sql = "DELETE FROM collection WHERE col_date <= curdate()"

        this.con.beginTransaction((err) => {
    
            if (err) { throw err; }
    
            this.con.query(sql, (err) => {
    
                if (err) { return false }
        
                console.log('entra')

            })
    
        });
    }


    //AUTHORS

    getAuthors(callback) {

        this.connectdb();

        fAuthor.getAuthors(this.con, callback);
    }

    getAuthor(id_aut, callback) {

        this.connectdb();

        fAuthor.getAuthor(this.con, id_aut, callback);
    }

    setAuthor(id_aut, author, img, callback) {

        this.connectdb();

        fAuthor.setAuthor(this.con, id_aut, author, img, callback);

    }

    setUserAuthor(author, callback) {

        this.connectdb();

        fAuthor.setUserAuthor(this.con, author, callback);

    }

    loginAuthor(credentials, callback) {

        this.connectdb();

        fAuthor.loginAuthor(this.con, credentials, callback);

    }

    //COLLECTIONS

    // ---- PENDENTS:

    getPendentCollection(id_pcol, callback) {

        this.connectdb();

        fCollection.getPendentCollection(this.con, id_pcol, callback);

    }

    getPendentCollectionByAuthor(id_author, callback) {

        this.connectdb();

        fCollection.getPendentCollectionByAuthor(this.con, id_author, callback);

    }


    getPendentCollections(callback) {

        this.connectdb();

        fCollection.getPendentCollections(this.con, callback);
    }

    validatePendentCollection(id_pcol, callback) {

        this.connectdb();

        fCollection.validatePendentCollection(this.con, id_pcol, callback);
    }

    setPendentCollection(id_author, pcol, callback) {

        this.connectdb();

        fCollection.setPendentCollection(this.con, id_author, pcol, callback);
    }

    updatePendentCollection(pcol, callback) {

        this.connectdb();

        fCollection.updatePendentCollection(this.con, pcol, callback)
    }


    // ------ validades:

    getCollection(id_col, callback) {

        this.connectdb();

        fCollection.getCollection(this.con, id_col, callback);
    }

    getCollections(callback) {

        this.connectdb();

        fCollection.getCollections(this.con, callback);
    }

    getOldCollections(callback) {

        this.connectdb();

        fCollection.getOldCollections(this.con, callback);
    }


    getActiveCollections(callback) {

        this.connectdb();

        fCollection.getActiveCollections(this.con, callback);
    }

    updateCollection(col, callback) {

        this.connectdb();

        fCollection.updateCollection(this.con, col, callback)
    }

    hasCollection(id_aut, callback) {
        this.connectdb();

        fCollection.hasCollection(this.con, id_aut, callback);
    }



    //ARTWORKS

    // ------ pendents:

    getPendentArtworks(id_pcol, callback) {

        this.connectdb();

        fArtwork.getPendentArtworks(this.con, id_pcol, callback);
    }

    getPendentArtwork(id_partw, callback) {

        this.connectdb();

        fArtwork.getPendentArtwork(this.con, id_partw, callback);

    }

    setPendentArtworks(id_pcol, partworks, callback) {

        this.connectdb();

        fArtwork.setPendentArtworks(this.con, id_pcol, partworks, callback);
    }

    updatePendentArtworks(id_pcol, partworks, callback) {

        this.connectdb();

        fArtwork.updatePendentArtworks(this.con, id_pcol, partworks, callback);
    }

    pendentArtworksToDelete(id_pcol, partworks, callback) {
        this.connectdb();

        fArtwork.pendentArtworksToDelete(this.con, id_pcol, partworks, callback);
    }

    // --- validades:

    getArtworks(id_col, callback) {

        this.connectdb();

        fArtwork.getArtworks(this.con, id_col, callback);
    }

    getArtwork(id_artw, callback) {

        this.connectdb();

        fArtwork.getArtwork(this.con, id_artw, callback);
    }

    updateArtworks(id_col, artworks, callback) {

        this.connectdb();

        fArtwork.updateArtworks(this.con, id_col, artworks, callback);
    }

    artworksToDelete(id_pcol, partworks, callback) {
        this.connectdb();

        fArtwork.artworksToDelete(this.con, id_pcol, partworks, callback);
    }


    // ADMIN

    loginAdmin(credentials, callback) {

        this.connectdb();

        fAdmin.loginAdmin(this.con, credentials, callback);

    }

    sendContactForm(form, callback) {

        this.connectdb();

        fAdmin.sendContactForm(this.con, form, callback);
    }

    sendAuthorCredentials(credentials, callback) {

        this.connectdb();

        fAdmin.sendAuthorCredentials(credentials, callback);
    }

    // ACCESS FILES

    saveAuthorFiles(fileName, dirName, oldPath, callback) {

        fFiles.saveAuthorFiles(fileName, dirName, oldPath, callback);
    }

    saveArtworksFiles(fileName, dirName, oldPath, callback) {

        fFiles.saveArtworksFiles(fileName, dirName, oldPath, callback);
    }

    updateArtworksFiles(fileName, dirName, oldPath, callback) {

        fFiles.updateArtworksFiles(fileName, dirName, oldPath, callback);
    }

    getPathToSave(fileName, dirName, status) {

        var path = fFiles.getPathToSave(fileName, dirName, status);

        return path
    }

    setDirName(path, dirName, callback) {

        fFiles.setDirName(path, dirName, callback)
    }

    removeFile(path, callback) {

        fFiles.removeFile(path, callback)
    }
}
