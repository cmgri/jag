const Artwork = require('../models/Artwork');

module.exports = {
    getPendentArtworks(con, id_pcol, callback) {

        var pendentArtworksList = [];
        var sql = "SELECT * FROM pendentartwork WHERE pcol_id = " + id_pcol;
        con.query(sql, (err, partworks) => {

            if (err) {
                con.end();
                callback(false)
            }

            partworks.forEach(partwork => {

                var aux = new Artwork(partwork.partw_id, id_pcol, partwork.partw_name, partwork.partw_desc, partwork.partw_img);

                pendentArtworksList.push(aux);

                var aux = null;

            })

            con.end();
            callback(pendentArtworksList);
        })
    },

    getPendentArtwork(con, id_pcol, partwork, callback) {

        var sql = "INSERT INTO pendentartwork (pcol_id, partw_name, partw_desc, partw_img) VALUES (" + id_pcol + ", '" + partwork.name + "', '" + partwork.desc + "', '" + partwork.img + "')";

        con.query(sql, (err, partwork) => {

            var aux = new Artwork(partwork.partw_id, partwork.partw_name, partwork.partw_desc, partwork.partw_img);
            con.end();
            callback(aux);

        });
    },

    setPendentArtworks(con, id_pcol, partworks, callback) {

        var res = false;

        con.beginTransaction(() => {

            partworks.forEach(partwork => {

                var sql = "INSERT INTO pendentartwork (pcol_id, partw_name, partw_desc, partw_img) VALUES (" + id_pcol + ", '" + partwork.name + "', '" + partwork.desc + "', '" + partwork.img + "')";

                con.query(sql, (err) => {

                    if (err) {
                        con.rollback(() => {
                            console.log("Artwork ja creada");
                            callback(res);
                        });

                    }
                });

            });

            con.commit((err) => {
                if (err) {
                    this.con.rollback(() => {
                        console.log("Artwork ja creada");
                        callback(res);
                    });

                } else {
                    console.log("Artwork creada");
                    res = true;
                    con.end();
                    callback(res);
                }

            });
        })

    },

    updatePendentArtworks(con, id_pcol, partworks, callback) {

        var res = false;

        con.getConnection((err, connection) => {

            connection.beginTransaction(() => {

                partworks.forEach(partwork => {

                    this.artworkExists(con, id_pcol, partwork, (sql) => {

                        connection.query(sql, (err) => {

                            if (err) {
                                connection.rollback(() => {
                                    console.log(err);
                                    callback(res);
                                });
                            }
                        });
                    })
                });

                connection.commit((err) => {
                    if (err) {
                        connection.rollback(() => {
                            console.log(err);
                            callback(res);
                        });

                    } else {
                        res = true;
                        callback(res);
                    }

                });
            });

        });
    },

    artworkExists(con, id_pcol, partwork, callback) {

        var sqlSel = "SELECT * from pendentartwork WHERE partw_id = " + partwork.id

        con.query(sqlSel, (err, col) => {

            if (err) {
                console.log(err)

            }

            if (col == "") {
                var sql = "INSERT INTO pendentartwork (pcol_id, partw_name, partw_desc, partw_img) VALUES (" + id_pcol + ", '" + partwork.name + "', '" + partwork.desc + "', '" + partwork.img + "')";

            } else {
                var sql = "UPDATE pendentartwork SET partw_name = '" + partwork.name + "', partw_desc = '" + partwork.desc + "', partw_img = '" + partwork.img + "' WHERE pcol_id = " + id_pcol + " AND partw_id = " + partwork.id;
            }

            callback(sql)
        })
    },

    pendentArtworksToDelete(con, id_pcol, artworks, callback) {

        var newIds = []
        var oldIds = []
        var toDelete = []
        var checked = false

        artworks.forEach(artwork => {
            newIds.push(artwork.id)
        })

        var sqlSel = "SELECT * from pendentartwork WHERE pcol_id = " + id_pcol;

        con.query(sqlSel, (err, cols) => {

            if (err) {
                console.log(err)
            }

            cols.forEach(col => {
                oldIds.push(col.partw_id)
            })


            if (oldIds.length > newIds.length) {
                oldIds.sort()
                newIds.sort()

                oldIds.forEach(oldId => {
                    newIds.forEach(newId => {
                        checked = oldId
                        if (newId == oldId) {
                            checked = true;
                        }
                    })
                    if (checked !== true) {
                        toDelete.push(checked)

                    }
                })


                toDelete.forEach(id => {

                    console.log(toDelete)
                    var sqlDel = "DELETE FROM pendentartwork WHERE partw_id = " + id;

                    con.query(sqlDel, (err) => {

                        if (err) {
                            console.log(err)
                        }

                        con.end();
                        callback()
                    });
                })

            } else {

            }
        })
    },


    // validades:

    getArtworks(con, id_col, callback) {

        var sql = "SELECT * FROM artwork WHERE col_id = " + id_col;
        con.query(sql, (err, artworks) => {
            var artworksList = [];
            artworks.forEach(artwork => {

                var aux = new Artwork(artwork.artw_id, id_col, artwork.artw_name, artwork.artw_desc, artwork.artw_img)

                artworksList.push(aux);

                var aux = null;

            })

            con.end();
            callback(artworksList);
        })
    },

    getArtwork(con, id_artw, callback) {

        var sql = "SELECT * FROM artwork WHERE artw_id = " + id_artw;

        con.query(sql, (err, artwork) => {

            var aux = new Artwork(artwork.artw_id, artwork.artw_name, artwork.artw_desc, artwork.artw_img);
            con.end();
            callback(aux);

        });
    },

    updateArtworks(con, id_col, artworks, callback) {

        var res = false;

        con.beginTransaction(() => {

            artworks.forEach(artwork => {

                var sql = "UPDATE artwork SET col_id=" + id_col + ", artw_name = '" + artwork.name + "', artw_desc = '" + artwork.desc + "', artw_img = '" + artwork.img + "' WHERE col_id = " + id_col + " AND artw_id = " + artwork.id;

                con.query(sql, (err) => {

                    if (err) {
                        con.rollback(() => {
                            console.log("Artwork ja creada");
                            con.end();
                            callback(res);
                        });

                    }
                });

            });

            con.commit((err) => {
                if (err) {
                    this.con.rollback(() => {
                        console.log("Artwork ja creada");
                        con.end();
                        callback(res);
                    });

                } else {
                    console.log("Artwork creada");
                    res = true;
                    con.end();
                    callback(res);
                }

            });
        })

    },

    artworksToDelete(con, id_col, artworks, callback) {

        var newIds = []
        var oldIds = []
        var toDelete = []
        var checked = false

        artworks.forEach(artwork => {
            newIds.push(artwork.id)
        })

        var sqlSel = "SELECT * from artwork WHERE col_id = " + id_col;

        con.query(sqlSel, (err, cols) => {

            if (err) {
                console.log(err)
            }

            cols.forEach(col => {
                oldIds.push(col.partw_id)
            })


            if (oldIds.length > newIds.length) {
                oldIds.sort()
                newIds.sort()

                oldIds.forEach(oldId => {
                    newIds.forEach(newId => {
                        checked = oldId
                        if (newId == oldId) {
                            checked = true;
                        }
                    })
                    if (checked !== true) {
                        toDelete.push(checked)
                    }
                })


                toDelete.forEach(id => {
                    var sqlDel = "DELETE FROM artwork WHERE artw_id = " + id;

                    con.query(sqlDel, (err) => {

                        if (err) {
                            console.log(err)
                        }

                        con.end();
                        callback()
                    });
                })

            } else {
                con.end();
                callback()
            }
        })
    },
}