const Author = require('../models/Author')

module.exports = {

    getAuthors(con, callback) {

        var sql = "SELECT * FROM author";
        con.query(sql, (err, authors) => {
            var autorsList = [];

            authors.forEach(author => {

                var aux = new Author(author.aut_id, author.aut_name, author.aut_surn, author.aut_email, "", author.aut_desc, author.aut_img)

                autorsList.push(aux);

                var aux = null;

            })

            con.end();
            callback(autorsList);
        })
    },

    getAuthor(con, id_aut, callback) {

        var sql = "SELECT * FROM author WHERE aut_id = '" + id_aut + "'";
        con.query(sql, (err, author) => {

            var aux = new Author(author[0].aut_id, author[0].aut_name, author[0].aut_surn, author[0].aut_email, "", author[0].aut_desc, author[0].aut_img)

            con.end();
            callback(aux)
        });
    },

    setAuthor(con, id_aut, author, img, callback) {

        var sql = "UPDATE author SET aut_name = '" + author.name + "', aut_surn = '" + author.surname + "', aut_desc = '" + author.desc + "', aut_img = '" + img + "' WHERE aut_id = '" + id_aut + "'";

        con.getConnection((err, connection) => {
            connection.beginTransaction((err) => {
                if (err) { throw err; }

                connection.query(sql, (err, author) => {

                    if (err) {
                        connection.rollback(() => { throw err; })
                    }

                    connection.commit((err) => {
                        if (err) {
                            connection.rollback(() => { throw err })
                        }

                        console.log("Transaction complete")
                        callback();

                    });
                });
            });
        });

    },

    setUserAuthor(con, author, callback) {

        console.log(author)

        var res = false;

        var sql = "INSERT INTO author (aut_email, aut_pass) VALUES ('" + author.email + "', '" + author.paswd + "')"

        con.beginTransaction(() => {

            con.query(sql, (err) => {

                if (err) {
                    con.rollback(() => {
                        console.log("The user elready exists");
                        con.end();
                        callback(res);
                    });

                } else {

                    con.commit((err) => {
                        if (err) {
                            con.rollback(() => {
                                console.log("The user elready exists");
                                con.end();
                                callback(res);
                            });

                        } else {
                            console.log("Author created");
                            res = true;
                            con.end();
                            callback(res);
                        }

                    });

                }
            });

        });

    },

    loginAuthor(con, credentials, callback) {

        var sql = "SELECT count(*) as cnt, aut_id FROM author WHERE aut_email = '" + credentials.email + "' AND aut_pass = '" + credentials.password + "'";
        con.query(sql, (err, res) => {


            if (res[0].cnt == 1) {
                res = res[0].aut_id;
            } else {

                console.log(err);
                res = false;
            }

            con.end();
            callback(res)
        })

    }

};
