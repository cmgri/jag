const Collection = require('../models/Collection');
const fArtwork = require('./FunctionsArtwork');

module.exports = {

    getPendentCollection(con, id_pcol, callback) {

        var sql = "SELECT * FROM pendentcollection WHERE pcol_id = " + id_pcol;
        con.query(sql, (err, pcol) => {

            var aux = new Collection(pcol[0].pcol_id, pcol[0].aut_id, pcol[0].pcol_name, pcol[0].pcol_desc, pcol[0].pcol_img)

            console.log(aux)
            callback(aux);
        });

    },

    getPendentCollectionByAuthor(con, id_author, callback) {

        var sql = "SELECT * FROM pendentcollection WHERE aut_id = " + id_author;
        con.query(sql, (err, pcol) => {

            var aux = new Collection(pcol[0].pcol_id, pcol[0].aut_id, pcol[0].pcol_name, pcol[0].pcol_desc, pcol[0].pcol_img)

            con.end();
            callback(aux);

        });
    },

    getPendentCollections(con, callback) {

        var pendentCollections = [];
        var sql = "SELECT * FROM pendentcollection";
        con.query(sql, (err, pcols) => {

            pcols.forEach(pcol => {

                var aux = new Collection(pcol.pcol_id, pcol.aut_id, pcol.pcol_name, pcol.pcol_desc, pcol.pcol_img, "", "", pcol.pcol_date)

                pendentCollections.push(aux);

                var aux = null;


            })

            con.end();
            callback(pendentCollections);

        });
    },

    validatePendentCollection(con, id_pcol, callback) {


        var sqlInsertCol = "INSERT INTO collection (aut_id, col_name, col_desc, col_img, col_actv) SELECT aut_id, pcol_name, pcol_desc, pcol_img, true FROM pendentcollection WHERE pcol_id = " + id_pcol;
        var sqlInsertArtw = "INSERT INTO artwork (col_id, artw_name, artw_desc, artw_img) SELECT pcol_id, partw_name, partw_desc, partw_img FROM pendentartwork WHERE pcol_id = " + id_pcol;
        var sqlDeleteCol = "DELETE FROM pendentcollection WHERE pcol_id = " + id_pcol;
        var sqlDeleteArtw = "DELETE FROM pendentartwork WHERE pcol_id = " + id_pcol;

        con.getConnection((err, connection) => {
            connection.beginTransaction((err) => {
                if (err) { throw err; }

                connection.query(sqlInsertCol, (err) => {

                    if (err) {
                        connection.rollback(() => { throw err; })
                    }

                    connection.query(sqlInsertArtw, (err) => {

                        if (err) {
                            connection.rollback(() => { throw err; })
                        }

                        connection.query(sqlDeleteCol, (err) => {

                            if (err) {
                                connection.rollback(() => { throw err; })
                            }

                            connection.query(sqlDeleteArtw, (err) => {

                                if (err) {
                                    connection.rollback(() => { throw err; })
                                }

                                connection.commit((err) => {

                                    if (err) {
                                        connection.rollback(() => { throw err })
                                    }

                                    console.log("Transaction complete")
                                    callback(true);

                                });

                            });

                        })

                    });

                });

            });
        })

    },

    setPendentCollection(con, id_author, pcol, callback) {

        var res = false;

        var sql = "INSERT INTO pendentCollection (aut_id, pcol_name, pcol_desc, pcol_img) VALUES (" + id_author + ", '" + pcol.name + "', '" + pcol.desc + "', '" + pcol.img + "')";

        con.getConnection((err, connection) => {
            connection.beginTransaction(() => {

                connection.query(sql, (err) => {

                    if (err) {
                        connection.rollback(() => {
                            console.log("Coleccio ja creada");
                            callback(res);
                        });

                    } else {

                        connection.commit((err) => {
                            if (err) {
                                connection.rollback(() => {
                                    console.log("Coleccio ja creada");
                                    callback(res);
                                });

                            } else {
                                console.log("Coleccio creada");

                                res = true;
                                callback(res);

                            }

                        });

                    }
                });
            });

        });
    },

    updatePendentCollection(con, pcol, callback) {

        var sql = "UPDATE pendentcollection SET pcol_name = '" + pcol.name + "', pcol_desc = '" + pcol.desc + "', pcol_img = '" + pcol.img + "' WHERE pcol_id = '" + pcol.id + "'";

        con.getConnection((err, connection) => {

            connection.beginTransaction((err) => {
                if (err) { throw err; }

                connection.query(sql, (err) => {

                    if (err) {
                        connection.rollback(() => { throw err; })
                    }

                    connection.commit((err) => {
                        if (err) {
                            connection.rollback(() => { throw err })
                        }

                        callback();
                    });

                });

            });
        });

    },

    //Validated

    getCollection(con, id_col, callback) {

        var sql = "SELECT * FROM collection WHERE col_id = " + id_col;
        con.query(sql, (err, col) => {

            var aux = new Collection(col[0].col_id, col[0].aut_id, col[0].col_name, col[0].col_desc, col[0].col_img, [], col[0].col_actv)

            con.end();
            callback(aux);


        });
    },

    getCollections(con, callback) {

        var collectionsList = [];
        var sql = "SELECT * FROM collection";

        con.query(sql, (err, cols) => {

            cols.forEach(col => {

                fArtwork.getArtworks(con, col[0].col_id, (artworks) => {

                    var aux = new Collection(col[0].col_id, col[0].aut_id, col[0].col_name, col[0].col_desc, col[0].col_img, artworks, col[0].col_actv);

                    collectionsList.push(aux);

                    var aux = null;

                });

            })

            con.end();
            callback(collectionsList);

        });
    },

    getOldCollections(con, callback) {

        var oldCollectionsList = [];
        var sql = "SELECT * FROM collection WHERE col_actv = false";

        con.query(sql, (err, cols) => {

            cols.forEach(col => {

                var aux = new Collection(col.col_id, col.aut_id, col.col_name, col.col_desc, col.col_img);

                oldCollectionsList.push(aux);

                var aux = null;

            })

            con.end();
            callback(oldCollectionsList);

        });
    },

    getActiveCollections(con, callback) {

        var activeCollectionsList = [];
        var sql = "SELECT * FROM collection WHERE col_actv = true";
        con.query(sql, (err, cols) => {

            cols.forEach(col => {

                var aux = new Collection(col.col_id, col.aut_id, col.col_name, col.col_desc, col.col_img)

                activeCollectionsList.push(aux);

                var aux = null;

            });

            con.end();
            callback(activeCollectionsList);

        });
    },

    updateCollection(con, col, callback) {

        console.log(col)

        var sql = "UPDATE collection SET col_name = '" + col.name + "', col_desc = '" + col.desc + "', col_img = '" + col.img + "' WHERE col_id = '" + col.id + "'";

        con.beginTransaction((err) => {
            if (err) { throw err; }

            con.query(sql, (err) => {

                if (err) {
                    con.rollback(() => { throw err; })
                }

                con.commit((err) => {
                    if (err) {
                        con.rollback(() => { throw err })
                    }

                    console.log("Transaction complete")
                    callback();

                });

            });

        });
    },

    hasCollection(con, id_aut, callback) {
        var sql = "SELECT * FROM pendentcollection WHERE aut_id = " + id_aut;
        res = false;
        con.query(sql, (err, col) => {

            if (col == "") {
                callback(res)

            } else {

                fArtwork.getPendentArtworks(con, col[0].pcol_id, (artworks) => {

                    var aux = new Collection(col[0].pcol_id, col[0].aut_id, col[0].pcol_name, col[0].pcol_desc, col[0].pcol_img, artworks, col[0].pcol_actv)

                    console.log(aux)

                    callback(aux);
                });
            }

        });
    }

};