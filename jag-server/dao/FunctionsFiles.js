const fs = require('fs')

module.exports = {

    saveArtworksFiles(fileName, dirName, oldPath, callback) {

        var path = [process.cwd(), 'public', 'resources', 'artworks', 'pendents', dirName, fileName]
        var pathDir = [process.cwd(), 'public', 'resources', 'artworks', 'pendents', dirName]
        var pathToSave = ['resources', 'artworks', 'pendents', dirName, fileName]

        fs.access(pathDir.join('/'), function (err) {
            if (err && err.code === 'ENOENT') {
                fs.mkdir(pathDir.join('/'), (err) => {
                    if (err) {
                        return console.error(err);
                    }
                });
            }

            fs.rename(oldPath, path.join('/'), ()=>{
                callback(pathToSave.join('/'))

            })
        });
    },

    updateArtworksFiles(fileName, dirName, oldPath, callback){
        var path = [process.cwd(), 'public', 'resources', 'artworks', 'validated', dirName, fileName]
        var pathDir = [process.cwd(), 'public', 'resources', 'artworks', 'validated', dirName]
        var pathToSave = ['resources', 'artworks', 'validated', dirName, fileName]

        fs.access(pathDir.join('/'), function (err) {
            if (err && err.code === 'ENOENT') {
                fs.mkdir(pathDir.join('/'), (err) => {
                    if (err) {
                        return console.error(err);
                    }
                });
            }

            fs.rename(oldPath, path.join('/'), ()=>{
                callback(pathToSave.join('/'))

            })
        });
    },

    saveAuthorFiles(fileName, dirName, oldPath, callback) {

        var path = [process.cwd(), 'public', 'resources', 'authors', 'pendents', dirName, fileName]
        var pathDir = [process.cwd(), 'public', 'resources', 'authors', 'pendents', dirName]
        var pathToSave = ['resources', 'authors', 'pendents', dirName, fileName]

        fs.access(pathDir.join('/'), function (err) {
            if (err && err.code === 'ENOENT') {
                fs.mkdir(pathDir.join('/'), (err) => {
                    if (err) {
                        return console.error(err);
                    }
                });
            }

            fs.rename(oldPath, path.join('/'), ()=>{
                callback(pathToSave.join('/'))

            })
            
        });
    },

    getPathToSave(fileName, dirName, status = 'pendents'){
        var pathToSave = ['resources', 'artworks', status, dirName, fileName]
        return pathToSave.join('/')
    },

    setDirName(path, dirName, callback){

        var pathArray = path.split('/')
        pathArray.pop()
        pathArray.pop()
        pathArray.push(dirName)
        var newPath = pathArray.join('/')
        
        fs.rename(path, newPath, ()=>{
            if(err) throw err;

            callback()
        });
    },

    removeFile(path){
        fs.unlink(path);
    }
}