
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const joi = require('joi');
const DAO = require('./dao/DAO');
const formidable = require('formidable');
const cron = require("node-cron");


const dao = new DAO();
const app = express();

app.use('/public', express.static(path.join(__dirname, 'static')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static('public'));

cron.schedule('0 5 * * *', () => {
   dao.checkActiveCollection()
});

//AUTHORS

app.get('/getAuthor/:id', (req, res) => {

    var id_author = req.params.id;
    dao.getAuthor(id_author, (author) => {

        res.json({ success: author });
    })
});

app.get('/getAuthors', (req, res) => {
    dao.getAuthors((authors) => {

        res.json({ success: true, authors });
    })
});

app.post('/setAuthor/:id', (req, res) => {

    var fields = [];
    var img = null;
    var id_author = req.params.id;

    var form = new formidable.IncomingForm();
    form.parse(req);

    form.on('fileBegin', (name, file) => {

        const profileImgSchema = joi.object().keys({
            pathf: joi.string().required()
        });

        var pathf = { pathf: file.name };

        joi.validate(pathf, profileImgSchema, (err, result) => {
            if (err) {
                console.log(err.details);
            } else {
                dao.saveAuthorFiles(file.name, id_author, file.path, (pathToSave) => {
                    img = pathToSave
                })

            }
        });
    })

    form.on('field', (name, field) => {
        fields.push(field);

    });


    form.on('end', () => {
        const schema = joi.object().keys({
            name: joi.string().required(),
            surname: joi.string().required(),
            desc: joi.string().required()
        });

        var name = fields[1];
        var surname = fields[2];
        var desc = fields[5];

        if (img == null) {
            img = fields[6];
        }

        var data = { name, surname, desc };

        joi.validate(data, schema, (err, result) => {
            if (err) {
                console.log(err.details);
            } else {
                dao.setAuthor(id_author, data, img, () => {
                    res.json({ success: true });
                });
            }
        });

    });

});

app.post('/setUserAuthor', (req, res) => {

    const schema = joi.object().keys({
        email: joi.string().email().required(),
        paswd: joi.string().required()
    });

    var email = req.body.email;
    var paswd = req.body.paswd;

    var data = { email, paswd };


    joi.validate(data, schema, (err, result) => {
        if (err) {
            res.send(err.details);
        } else {
            dao.setUserAuthor(data, (result) => {
                dao.sendAuthorCredentials(data, ()=>{
                    res.send(true);
                })
            });
        }
    })
});

app.post('/loginAuthor', (req, res) => {

    const schema = joi.object().keys({
        email: joi.string().email().required(),
        password: joi.string().required()
    });

    var email = req.body.email;
    var password = req.body.password;

    var data = { email, password };


    joi.validate(data, schema, (err, result) => {
        if (err) {
            res.send(err);
        }
        dao.loginAuthor(data, (result) => {
            if (!result) {
                res.json({ success: false });
            } else {
                res.json({ success: result });
            }

        });
    })
});


//COLLECTIONS

// ---- Pendents:

app.get('/getPendentCollection/:id', (req, res) => {

    var id = req.params.id;

    dao.getPendentCollection(id, (collection) => {
        res.json(collection);
    })
});

app.get('/getPendentCollections', (req, res) => {

    dao.getPendentCollections((pendentCollections) => {
        res.json(pendentCollections);
    })
});

app.post('/setPendentCollection/:id', (req, res) => {

    var fields = [];
    var img = null;
    var id_author = req.params.id;

    var form = new formidable.IncomingForm();
    form.parse(req);

    form.on('fileBegin', (name, file) => {

        const imgSchema = joi.object().keys({
            path: joi.string().required()
        });

        var path = { path: file.name };

        joi.validate(path, imgSchema, (err, result) => {
            if (err) {
                console.log(err.details);
            } else {
                dao.saveArtworksFiles(file.name, 'temp' + id_author, file.path, (pathToSave) => {
                    img = pathToSave
                })
            }
        });
    })

    form.on('field', (name, field) => {
        fields.push(field);

    });

    form.on('end', () => {
        const schema = joi.object().keys({
            name: joi.string().required(),
            desc: joi.string().required(),
            imgName: joi.string().required(),

        });

        var name = fields[0];
        var desc = fields[1];
        var imgName = fields[2];

        var data = { name, desc, imgName };

        joi.validate(data, schema, (err, result) => {
            if (err) {
                console.log(err.details);
            } else {
                data.img = img
                dao.setPendentCollection(id_author, data, () => {
                    res.json({ success: true });
                })
            }
        });

    });


});

app.get('/validatePendentCollection/:id', (req, res) => {
    var id_collection = req.params.id;

    dao.validatePendentCollection(id_collection, (result) => {
        res.json(result);
    })
});

app.post('/updatePendentCollection/:id', (req, res) => {

    var fields = [];
    var img = null;
    var id_author = req.params.id;

    var form = new formidable.IncomingForm();
    form.parse(req);

    form.on('fileBegin', (name, file) => {

        const imgSchema = joi.object().keys({
            pathf: joi.string().required()
        });

        var pathf = { pathf: file.name };

        joi.validate(pathf, imgSchema, (err, result) => {
            if (err) {
                console.log(err.details);
            } else {

                dao.saveArtworksFiles(file.name, 'temp' + id_author, file.path, (pathToSave) => {
                    img = pathToSave
                })
            }
        });
    })

    form.on('field', (name, field) => {

        fields.push(field);

    });

    form.on('end', () => {
        const schema = joi.object().keys({
            id: joi.string().required(),
            name: joi.string().required(),
            desc: joi.string().required()
        });

        var id = fields[0]
        var name = fields[2];
        var desc = fields[3];

        if (img == null) {
            img = fields[4];
        }

        var data = { id, name, desc };

        joi.validate(data, schema, (err, result) => {
            if (err) {
                console.log(err.details);
            } else {
                data.img = img;
                dao.updatePendentCollection(data, () => {
                });
            }
        });

    });
});

app.get('/hasCollection/:id', (req, res) => {

    var id_author = req.params.id;
    dao.hasCollection(id_author, (result) => {
        console.log(result)
        res.json({ success: result });
    })
});


// --- Validades:

app.get('/getCollection/:id', (req, res) => {

    var id_collection = req.params.id;

    dao.getCollection(id_collection, (collection) => {
        res.json(collection);
    })
});

app.get('/getCollections', (req, res) => {

    dao.getCollections((collections) => {
        res.json(collections);
    })
});

app.get('/getOldCollections', (req, res) => {

    dao.getOldCollections((oldCollections) => {

        res.json(oldCollections);
    })
});

app.get('/getActiveCollections', (req, res) => {

    dao.getActiveCollections((activeCollections) => {

        res.json(activeCollections);
    })
});

app.post('/updateCollection/:id', (req, res) => {

    var fields = [];
    var img = null;
    var id_col = req.params.id;

    var form = new formidable.IncomingForm();
    form.parse(req);

    form.on('fileBegin', (name, file) => {

        const imgSchema = joi.object().keys({
            path: joi.string().required()
        });

        var path = { path: file.name };

        joi.validate(path, imgSchema, (err, result) => {
            if (err) {
                console.log(err.details);
            } else {
                dao.updateArtworksFiles(file.name, id_col, file.path, (pathToSave) => {
                    img = pathToSave
                })
            }
        });
    })

    form.on('field', (name, field) => {
        fields.push(field);

    });

    form.on('end', () => {
        console.log(fields)
        const schema = joi.object().keys({
            id: joi.string().required(),
            name: joi.string().required(),
            desc: joi.string().required()
        });

        var id = fields[0]
        var name = fields[2];
        var desc = fields[3];

        if (img == null) {
            img = fields[4];
        }

        var data = { id, name, desc };

        joi.validate(data, schema, (err, result) => {
            if (err) {
                console.log(err.details);
            } else {
                data.img = img;
                dao.updateCollection(data, () => {
                    res.json({ success: true });
                });
            }
        });

    });
});


//Artworks:

app.get('/getArtworks/:id', (req, res) => {

    var id_collection = req.params.id;

    dao.getArtworks(id_collection, (artworks) => {
        res.json(artworks);
    })
});

app.get('/getPendentArtworks/:id', (req, res) => {

    var id_collection = req.params.id;

    dao.getPendentArtworks(id_collection, (artworks) => {
        res.json(artworks);
    })
});

app.post('/setPendentArtwork/:id', (req, res) => {

    var fields = [];
    var imgsPath = [];
    var artworks = [];
    var id_author = req.params.id;

    var form = new formidable.IncomingForm();
    form.parse(req);

    form.on('fileBegin', (name, file) => {

        const artworkSchema = joi.object().keys({
            path: joi.string().required()
        });

        var path = file.name;

        joi.validate(path, artworkSchema, (err, result) => {
            if (err) {
                console.log(err.details);
            } else {
                dao.saveArtworksFiles(file.name, 'temp' + id_author, file.path, (pathToSave) => {
                    imgsPath.push(pathToSave)
                })
            }
        });

    });

    form.on('field', (name, field) => {
        fields.push(field);

    });

    form.on('end', () => {
        const schema = joi.object().keys({
            name: joi.string().required(),
            desc: joi.string().required(),
            imgName: joi.string().required()
        });

        for (let index = 0; index < fields.length; index += 6) {

            var name = fields[index + 2];
            var desc = fields[index + 3];
            var imgName = fields[index + 5];

            var data = { name, desc, imgName };

            joi.validate(data, schema, (err, result) => {
                if (err) {
                    console.log(err.details);
                } else {

                    artworks.push({})
                    artworks[index].name = name;
                    artworks[index].desc = desc;
                    var path = dao.getPathToSave(imgName, 'temp' + id_author);
                    artworks[index].img = path

                    if (index == (fields.length - 6)) {

                        console.log(artworks)
                        dao.getPendentCollectionByAuthor(id_author, (col) => {
                            dao.setPendentArtworks(col.pcol_id, artworks, () => {
                                res.json({ success: true });
                            });
                        })

                    }
                }
            });

        }

    });


});

app.post('/updatePendentArtworks/:id', (req, res) => {

    var fields = [];
    var imgs = [];
    var imgsPath = []
    var artworks = []
    var id_author = req.params.id;
    var i = 0

    var form = new formidable.IncomingForm();
    form.parse(req);

    form.on('fileBegin', (name, file) => {

        const artworkSchema = joi.object().keys({
            path: joi.string().required()
        });

        var path = { path: file.name };

        joi.validate(path, artworkSchema, (err, result) => {
            if (err) {
                console.log(err.details);
            } else {
                dao.saveArtworksFiles(file.name, 'temp' + id_author, file.path, (pathToSave) => {
                    imgsPath.push(pathToSave)
                })
            }
        });
    })

    form.on('field', (name, field) => {
        fields.push(field);

    });

    form.on('end', () => {
        const schema = joi.object().keys({
            id: joi.string().required(),
            idCol: joi.string().required(),
            name: joi.string().required(),
            desc: joi.string().required(),
            imgName: joi.string().required()
        });

        for (let index = 0; index < fields.length; index += 6) {

            var id = fields[index];
            var idCol = fields[index + 1];
            var name = fields[index + 2];
            var desc = fields[index + 3];
            var imgName = fields[index + 5];

            var data = { id, idCol, name, desc, imgName };

            joi.validate(data, schema, (err, result) => {
                if (err) {
                    console.log(err.details);
                } else {

                    artworks.push({})
                    artworks[i].id = id;
                    artworks[i].idCol = idCol;
                    artworks[i].name = name;
                    artworks[i].desc = desc;
                    var path = dao.getPathToSave(imgName, 'temp' + id_author);
                    artworks[i].img = path

                    if (index == (fields.length - 6)) {
                        dao.updatePendentArtworks(idCol, artworks, () => {
                            dao.pendentArtworksToDelete(idCol, artworks, () => {
                                res.json({ success: true });
                            })
                        });

                    }
                }
            })

            i++
        }



    });


});

app.post('/updateArtworks/:id', (req, res) => {

    var fields = [];
    var imgs = [];
    var imgsPath = []
    var artworks = []
    var id_col = req.params.id;
    var i = 0

    var form = new formidable.IncomingForm();
    form.parse(req);

    form.on('fileBegin', (name, file) => {

        const artworkSchema = joi.object().keys({
            path: joi.string().required()
        });

        var path = { path: file.name };

        joi.validate(path, artworkSchema, (err, result) => {
            if (err) {
                console.log(err.details);
            } else {
                dao.updateArtworksFiles(file.name, id_col, file.path, (pathToSave) => {
                    console.log(imgsPath)
                    imgsPath.push(pathToSave)
                })
            }
        });
    })

    form.on('field', (name, field) => {
        fields.push(field);

    });

    form.on('end', () => {
        const schema = joi.object().keys({
            id: joi.string().required(),
            idCol: joi.string().required(),
            name: joi.string().required(),
            desc: joi.string().required(),
            imgName: joi.string().required()
        });

        console.log(fields)

        for (let index = 0; index < fields.length; index += 6) {

            var id = fields[index];
            var idCol = fields[index + 1];
            var name = fields[index + 2];
            var desc = fields[index + 3];
            var imgName = fields[index + 5];

            var data = { id, idCol, name, desc, imgName };

            joi.validate(data, schema, (err, result) => {
                if (err) {
                    console.log(err.details);
                } else {

                    artworks.push({})
                    artworks[i].id = id;
                    artworks[i].idCol = idCol;
                    artworks[i].name = name;
                    artworks[i].desc = desc;
                    var path = dao.getPathToSave(imgName, id_col, 'validated');
                    artworks[i].img = path

                    if (index == (fields.length - 6)) {
                        dao.updateArtworks(idCol, artworks, () => {
                            dao.artworksToDelete(idCol, artworks, () => {
                                res.json({ success: true });
                            })
                        });

                    }
                }
            })

            i++
        }



    });


});

//ADMINS:

app.post('/loginAdmin', (req, res) => {

    const schema = joi.object().keys({
        email: joi.string().email().required(),
        password: joi.string().required()
    });

    var email = req.body.email;
    var password = req.body.password;

    var data = { email, password };


    joi.validate(data, schema, (err, result) => {
        if (err) {
            res.send(err);
        } else {
            dao.loginAdmin(data, (result) => {
                if (!result) {
                    //req.session.loggedin = true;
                    res.json({ success: false });
                } else {
                    res.json({ success: result });
                }

            });
        }

    })
});

app.post('/sendContactForm', (req, res) => {


    const schema = joi.object().keys({

        name: joi.string().required(),
        email: joi.string().email().required(),
        info: joi.string().required()

    });

    var name = req.body.name;
    var email = req.body.email;
    var info = req.body.info;

    var data = { name, email, info };

    console.log(data)

    joi.validate(data, schema, (err, result) => {
        if (err) {
            res.send(err);
        }
        dao.sendContactForm(data, (result) => {
            if (result) {
                res.json({ success: true });
            } else {
                res.json({ success: false });
            }

        });
    })
})

app.listen(process.env.PORT || 5000);
