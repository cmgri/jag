module.exports = class Artwork {

    constructor (id, id_col, name, desc = "", img = ""){
        this.id = id;
        this.id_col = id_col;
        this.name = name;
        this.desc = desc;
        this.img = img;
    }

}