module.exports = class Author {

    constructor(id, name, surname, email, passw, desc = "", img = ""){
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.passw = passw;
        this.desc = desc;
        this.img = img;
    }
}