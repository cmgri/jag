module.exports = class Collection {
    constructor (id, id_aut, name, desc = "", img = "", artworks = [], active = false, p_date = "",){
        this.id = id;
        this.id_aut = id_aut;
        this.name = name;
        this.desc = desc;
        this.img = img;
        this.artworks = artworks;
        this.active = active;
        this.p_date = p_date;
    }
} 