import { viewAdminPanel } from './viewAdminPanel.mjs'

export class controllerAdminPanel {

    constructor(view) {

        this.view = view;

        this.authors = []
        this.activeCollections = []
        this.pendentCollections = []
        this.oldCollections = []
        this.userAuthor = {}

        this.rowNum = 0
        this.isPendent = false
        this.isOld = false

        this.currentCollection = ""
        this.currentArtworks = ""
        this.currentAuthor = ""


        $('document').ready(() => {

            this.bindMenuLinks()

            this.getAutors()
            this.getActiveCollections()
            this.getPendentCollections()
            this.getOldCollections()
        })
    }


    // BINDING

    bindMenuLinks() {
        $('#activeCol').click((e) => {
            this.view.setSelected(e.target)
            this.populatePanelActiveCollections()
        })

        $('#pendentCol').click((e) => {
            this.view.setSelected(e.target)
            this.populatePanelPendentCollections()
        })

        $('#authorsMng').click((e) => {
            this.view.setSelected(e.target)
            this.populatePanelAuthorsManager()
        })

        $('#oldCol').click((e) => {
            this.view.setSelected(e.target)
            this.populatePanelOldCollections()
        })
    }

    bindValidateBtn() {
        $('button[id^=btnValidate]').click((e) => {
            var row = $(e.target).parent().parent().attr('id')
            var name = $('#' + row + ' td[id^=tdName]').text()
            var collection = this.getCollectionByName(this.pendentCollections, name)

            this.validateCollection(collection)

        })
    }

    bindAddBtn() {
        $('#btnNew').click((e) => {
            this.view.createPanelAddAuthor()
            this.bindSendBtn()
        })
    }

    bindDetailBtns() {
        $('button[id^=btnDetail]').click((e) => {

            var row = $(e.target).parent().parent().attr('id')
            var name = $('#' + row + ' td[id^=tdName]').text()

            if ($(e.target).hasClass('btn-pendent')) {
                this.isPendent = true
                this.isOld = false
                this.isNew = false
                var collection = this.getCollectionByName(this.pendentCollections, name)
                this.setCurrentCollecion(collection)
                this.getArtworks('pendent')
            } else if ($(e.target).hasClass('btn-old')) {
                this.isOld = true
                this.isPendent = false
                this.isNew = false
                var collection = this.getCollectionByName(this.oldCollections, name)
                this.setCurrentCollecion(collection)
                this.getArtworks()
            } else {
                this.isPendent = false
                this.isOld = false
                this.isNew = false
                var collection = this.getCollectionByName(this.activeCollections, name)
                this.setCurrentCollecion(collection)
                this.getArtworks()
            }
            this.populatePanelEditCollection()
        })
    }

    bindAuthorDetailBtns() {
        $('button[id^=btnAuthorDetail').click((e) => {

            var row = $(e.target).parent().parent().attr('id')
            var email = $('#' + row + ' td[id^=tdEmail]').text()

            var author = this.getAuthorByEmail(email)
            this.setCurrentAuthor(author)
            this.populatePanelEditAuthor()
        })

    }

    bindSendBtn() {
        $('#btnSend').click(() => {
            this.saveUserAuthor()
            this.setUserAuthor()
        })
    }

    bindSaveAuthorBtn() {
        $('#btnSave').click((e) => {
            this.saveCurrentAuthor()
        })
    }

    bindFormTabs() {

        $('#linkCol').click(() => {

            $('#linkCol').addClass('active')
            $('#linkArtw').removeClass('active')

            this.view.resetForm()
            this.view.removeAllRows()
            this.rowNum = 0
            this.populateCollectionForm()
        })

        $('#linkArtw').click(() => {

            $('#linkArtw').addClass('active')
            $('#linkCol').removeClass('active')

            this.view.resetForm()
            this.view.createPanelEditArtworks(this.currentCollection.artworks.length)
            this.rowNum = (this.currentCollection.artworks.length - 1)
            this.populateArtworkForm()
        })

        $('#btnSave').click((e) => {
            if ($(e.target).parent().siblings('ul').children().children('.active').text() == 'Collection') {
                this.saveCurrentCollection()
            } else {
                this.saveCurrentArtworks()
            }
        })

    }

    bindArtwBtns(id = "") {

        $('#add-button').unbind('click');

        $('#add-button').click((e) => {
            var id = this.rowNum
            this.view.createInnerForm(id, true);
            this.bindArtwBtns(id);
            this.rowNum++
        })

        $('#delete-button' + id).click((e) => {
            if ($(e.target).attr('id') == 'delete-button') {
                var formContainer = e.target.parentElement.parentElement.parentElement;
                this.removeCurrentArtwork(formContainer);
                this.view.resetForm();
            } else {
                this.rowNum--
                if (this.rowNum <= 0) {
                    this.rowNum = ""
                }
                $("#addCol").append($('#add-button'))
                var formContainer = e.target.parentElement.parentElement.parentElement;
                this.removeCurrentArtwork(formContainer)
                formContainer.remove()
                $("div[id^=addCol]").last().append($('#add-button'))
            }
        })
    }

    bindFileInput(){
        $('.custom-file-input').unbind('change')
        $('.custom-file-input').change((e)=>{
            var file = $(e.target)[0].files[0].name;
            $(e.target).next('label').text(file);
        })
    }


    // GET REQUESTS

    getAutors() {
        $.ajax({
            url: '/getAuthors',
            type: 'get',
            success: (response) => {
                if (!response) {
                    this.view.createErrorModal()
                    $('.modal').modal()
                } else {
                    this.authors = response.authors
                }
            }
        })
    }

    getActiveCollections() {
        $.ajax({
            url: '/getActiveCollections',
            type: 'get',
            success: (response) => {
                if (!response) {
                    this.view.createErrorModal()
                    $('.modal').modal()
                } else {

                    this.activeCollections = response
                    this.populatePanelActiveCollections()
                }
            }
        })
    }

    getPendentCollections() {
        $.ajax({
            url: '/getPendentCollections',
            type: 'get',
            success: (response) => {
                if (!response) {
                    this.view.createErrorModal()
                    $('.modal').modal()
                } else {
                    this.pendentCollections = response
                }
            }
        })
    }

    getOldCollections() {
        $.ajax({
            url: '/getOldCollections',
            type: 'get',
            success: (response) => {
                if (!response) {
                    this.view.createErrorModal()
                    $('.modal').modal()
                } else {

                    this.oldCollections = response
                }
            }
        })
    }

    getArtworks(status = 'validated') {
        if (this.currentCollection.artworks.length == 0) {
            if (status == 'validated') {
                $.ajax({
                    url: '/getArtworks/' + this.currentCollection.id,
                    type: 'get',
                    success: (response) => {
                        if (!response) {
                            this.view.createErrorModal()
                            $('.modal').modal()
                        } else {
                            this.currentCollection.artworks = response
                        }
                    }
                })
            } else {
                $.ajax({
                    url: '/getPendentArtworks/' + this.currentCollection.id,
                    type: 'get',
                    success: (response) => {
                        if (!response) {
                            this.view.createErrorModal()
                            $('.modal').modal()
                        } else {

                            this.currentCollection.artworks = response
                        }
                    }
                })
            }
        }
    }


    // GETTERS

    getAuthorName(id) {

        var name = ""
        var surname = ""

        this.authors.forEach(author => {

            if (author.id == id) {
                name = author.name
                surname = author.surname
            }
        })

        return name + ' ' + surname
    }

    getAuthorByEmail(email) {
        var authorToSave = []

        this.authors.forEach(author => {

            if (author.email == email) {
                authorToSave = author
            }
        })

        return authorToSave
    }

    getCollectionById(collections, id) {
        var collectionToSave = []

        collections.forEach(collection => {

            if (collection.id == id) {
                collectionToSave = collection
            }
        })

        return collectionToSave
    }

    getCollectionByName(collections, name) {
        var collectionToSave = []

        collections.forEach(collection => {

            if (collection.name == name) {
                collectionToSave = collection
            }
        })

        return collectionToSave
    }

    getImgName(img) {
        var aux = img.split('/');
        var imgName = aux[aux.length]

        return imgName
    }

    getData(id = "") {
        var name = $('#inputName' + id).val()
        var desc = $('#inputDesc' + id).val()
        var img = document.getElementById('customFile' + id).files[0];

        if (img == undefined) {
            img = $('#labelFile' + id).text()
        }

        if ($('#inputSurname').is(':visible')) {
            var surname = $('#inputSurname').val();
            return { name: name, surname: surname, desc: desc, img: img }
        }

        return { name, desc, img }
    }


    // SETTERS

    setCurrentCollecion(collection = {}) {
        this.currentCollection = collection

    }

    setCurrentAuthor(author) {
        this.currentAuthor = author

    }


    // SAVINGS

    saveCurrentCollection() {

        var newValues = this.getData()

        this.currentCollection.name = newValues.name;
        this.currentCollection.desc = newValues.desc;
        this.currentCollection.img = newValues.img;

        var imgName = newValues.img

        if (typeof imgName !== "object") {
            var aux = imgName.split("/");
            this.currentCollection.imgName = aux[aux.length - 1]
        } else {
            this.currentCollection.imgName = newValues.img.name
        }
        console.log()

        this.sendCollection("NO OBJ "+this.currentCollection.imgName)
        this.sendCollection("OBJ "+this.currentCollection.imgName)

    }

    saveCurrentArtworks() {

        var rows = $('.form-container');
        var img = undefined

        rows.each((i) => {

            var id = '#' + $(rows[i]).attr('id');

            var name = $(id + ' input[id^=inputName]').val();
            var desc = $(id + ' textarea[id^=inputDesc]').val();
            img = document.querySelector(id + ' input[id^=customFile]').files[0];


            if (this.currentCollection.artworks[i] == undefined) {
                this.currentCollection.artworks.push({})
                this.currentCollection.artworks[i].id = 0;
                if (this.currentCollection.id !== undefined) {
                    this.currentCollection.artworks[i].id_col = this.currentCollection.id;
                } else {
                    this.currentCollection.artworks[i].id_col = 0
                }

            }

            this.currentCollection.artworks[i].name = name;
            this.currentCollection.artworks[i].desc = desc;

            if (img !== undefined) {
                this.currentCollection.artworks[i].img = img;
                this.currentCollection.artworks[i].control = "0";
                this.currentCollection.artworks[i].imgName = img.name;
            } else {
                var imgName = this.currentCollection.artworks[i].img.split("/");
                this.currentCollection.artworks[i].imgName = imgName[imgName.length - 1]
            }
        })

        this.sendArtworks()

    }

    saveCurrentAuthor() {

        var newValues = this.getData()

        this.currentAuthor.name = newValues.name;
        this.currentAuthor.surname = newValues.surname;
        this.currentAuthor.desc = newValues.desc;
        this.currentAuthor.img = newValues.img;

        var imgName = newValues.img

        if (typeof imgName !== "object") {
            var aux = imgName.split("/");
            this.currentAuthor.imgName = aux[aux.length - 1]
        } else {
            this.currentAuthor.imgName = newValues.img.name
        }

        this.sendAuthor()

    }

    removeCurrentArtwork(formContainer) {

        var containerid = formContainer.getAttribute('id')
        var name = $('#' + containerid + ' input[id^=inputName]').val()

        var i = 0;
        var toDelete = "";

        this.currentCollection.artworks.forEach(artwork => {
            if (artwork.name == name) {
                toDelete = i;
            }
            i++
        })

        delete this.currentCollection.artworks[toDelete];
    }

    saveUserAuthor() {
        this.userAuthor.email = $('#inputEmail').val()
        this.userAuthor.paswd = $('#inputPaswd').val()

    }


    //POPULATE CONTAINERS

    populateTableHeader(titles) {

        var spaces = $('thead > tr th')
        var i = 0

        titles.forEach(title => {
            $(spaces[i]).text(title)
            i++
        })

    }

    populatePanelActiveCollections() {

        var i = 0;

        this.view.createPanelActiveCollections(this.activeCollections.length)

        this.populateTableHeader(['#', 'Name', 'Author', 'Cover Image'])

        this.activeCollections.forEach(collection => {

            $('#tdName' + i).text(collection.name);
            $('#tdAuthor' + i).text(this.getAuthorName(collection.id_aut));
            $('#img' + i).attr('src', collection.img);

            i++

        })
        this.bindDetailBtns()

    }

    populatePanelPendentCollections() {

        var i = 0;

        this.view.createPanelPendentCollections(this.pendentCollections.length)

        this.populateTableHeader(['#', 'Name', 'Author', 'Upload Date', 'Cover Image'])

        this.pendentCollections.forEach(collection => {

            $('#tdName' + i).text(collection.name);
            $('#tdAuthor' + i).text(this.getAuthorName(collection.id_aut));
            var date = new Date(collection.p_date);
            var dateToShow = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
            $('#tdDate' + i).text(dateToShow);
            $('#img' + i).attr('src', collection.img);

            i++

        })

        this.bindValidateBtn()

        this.bindDetailBtns()

    }

    populatePanelAuthorsManager() {

        var i = 0;

        this.view.createPanelAuthorsManager(this.authors.length)

        this.populateTableHeader(['#', 'Name', 'E-mail', 'Profile Image'])

        this.authors.forEach(author => {

            $('#tdName' + i).text(author.name + ' ' + author.surname);
            $('#tdEmail' + i).text(author.email);
            $('#img' + i).attr('src', author.img);

            i++

        })

        this.bindAddBtn()
        this.bindAuthorDetailBtns();

    }

    populatePanelOldCollections() {

        var i = 0;

        this.view.createPanelOldCollections(this.oldCollections.length)

        this.populateTableHeader(['#', 'Name', 'Author', 'Cover Image'])

        this.oldCollections.forEach(collection => {

            $('#tdName' + i).text(collection.name);
            $('#tdAuthor' + i).text(this.getAuthorName(collection.id_aut));
            $('#img' + i).attr('src', collection.img);

            i++

        })

        this.bindDetailBtns()
    }

    populatePanelEditCollection() {

        this.view.createPanelEditCollection()
        this.bindFormTabs()
        this.populateCollectionForm()

        if (this.isOld) {
            this.view.disableInputs()
        }

        this.bindFileInput()
    }

    populatePanelEditAuthor() {

        this.view.createPanelEditAuthor()
        this.populateAuthorForm()
        this.bindSaveAuthorBtn()
        this.bindFileInput()
    }

    populatePanelDetailsOldCollection() {

        this.view.createPanelDetailsOldCollections()
        this.bindFormTabs()
        this.populateOldCollectionForm()
    }


    // POPULATE FORMS

    populateCollectionForm() {

        $('#inputName').val(this.currentCollection.name)
        $('#inputDesc').val(this.currentCollection.desc)
        $('#img').attr('src', this.currentCollection.img);
        $('#labelFile').text(this.currentCollection.img);

    }

    populateArtworkForm() {

        var i = -1
        var id = ""

        this.currentCollection.artworks.forEach(artwork => {

            if (i !== -1) {
                id = i
            }

            $('#inputName' + id).val(artwork.name)
            $('#inputDesc' + id).val(artwork.desc)
            $('#input' + id).val(artwork.desc)
            $('#labelFile' + id).text(artwork.img);
            $('#img' + id).attr('src', artwork.img);

            i++

            this.bindArtwBtns(id)
        })

        this.errorImg()
        this.bindFileInput()

    }

    errorImg() {

        var i = -1
        var id = ""

        this.currentCollection.artworks.forEach(artwork => {

            if (i !== -1) {
                id = i
            }
            console.log( $('#img' + id))
            $('#img' + id).attr('src', artwork.img);

            i++
        })
    }

    populateAuthorForm() {

        $('#inputName').val(this.currentAuthor.name)
        $('#inputSurname').val(this.currentAuthor.surname)
        $('#inputDesc').val(this.currentAuthor.desc)
        $('#img').attr('src', this.currentAuthor.img);
        $('#labelFile').text(this.currentAuthor.img);
    }


    // POST REQUESTS

    sendCollection() {

        var formDataCollection = new FormData();

        for (let [key, value] of Object.entries(this.currentCollection)) {
            formDataCollection.append(key, value)
        }

        formDataCollection.delete('artworks')

        if (!this.isPendent) {
            $.ajax({
                url: '/updateCollection/' + this.currentCollection.id,
                type: 'POST',
                data: formDataCollection,
                processData: false,
                contentType: false,
                success: (response) => {
                    if (response.success) {
                        this.view.createSuccessAlert('Submited')
                    } else {
                        this.view.createErrorModal()
                        $('.modal').modal()
                    }
                }
            });
        } else {
            $.ajax({
                url: '/updatePendentCollection/' + this.currentCollection.id_aut,
                type: 'POST',
                data: formDataCollection,
                processData: false,
                contentType: false,
                success: (response) => {

                    if (response.success) {
                        this.view.createSuccessAlert('Submited')
                    } else {
                        this.view.createErrorModal()
                        $('.modal').modal()
                    }
                }
            });
        }

    }

    sendArtworks() {

        var formDataArtworks = new FormData();

        this.currentCollection.artworks.forEach(artwork => {
            for (let [key, value] of Object.entries(artwork)) {
                formDataArtworks.append(key, value)
            }
        })

        if (!this.isPendent) {

            $.ajax({
                url: '/updateArtworks/' + this.currentCollection.id,
                type: 'POST',
                data: formDataArtworks,
                processData: false,
                contentType: false,
                success: (response) => {

                    if (response.success) {
                        this.view.createSuccessAlert('Submited')
                    } else {
                        this.view.createErrorModal()
                        $('.modal').modal()
                    }
                }
            });
        } else {
            $.ajax({
                url: '/updatePendentArtworks/' + this.currentCollection.id_aut,
                type: 'post',
                data: formDataArtworks,
                processData: false,
                contentType: false,
                success: (response) => {
                    if (response.success) {
                        this.view.createSuccessAlert('Submited')
                    } else {
                        this.view.createErrorModal()
                        $('.modal').modal()
                    }
                }
            });


        }

    }

    sendAuthor() {

        var formDataAuthor = new FormData();

        for (let [key, value] of Object.entries(this.currentAuthor)) {
            formDataAuthor.append(key, value)
        }

        $.ajax({
            url: '/setAuthor/' + this.currentAuthor.id,
            type: 'POST',
            data: formDataAuthor,
            processData: false,
            contentType: false,
            success: (response) => {

                if (response.success) {
                    this.view.removeAuthorRows()
                    this.view.createSuccessAlert('Submited')

                } else {
                    this.view.createErrorModal()
                    $('.modal').modal()
                }
            }
        });

    }

    validateCollection(collection) {

        $.ajax({
            url: '/validatePendentCollection/' + collection.id,
            type: 'get',
            success: (response) => {
                if (!response) {
                    this.view.createErrorModal()
                    $('.modal').modal()
                } else {
                    this.view.createSuccessAlert('Submited')
                    window.location.href = '/adminPanel.html';
                }
            }
        });

    }

    setUserAuthor() {

        console.log(this.userAuthor)

        $.ajax({
            url: '/setUserAuthor',
            type: 'POST',
            data: this.userAuthor,
            success: (response) => {

                if (response.success) {
                    this.view.createSuccessAlert('Submited')
                } else {
                    this.view.createErrorModal()
                    $('.modal').modal()
                }
            }
        });

    }

}

const app = new controllerAdminPanel(new viewAdminPanel());