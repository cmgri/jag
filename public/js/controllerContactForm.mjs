import {viewContact} from './viewContactForm.mjs'

export class controllerContactForm {
    constructor(view){

        this.view = view;

        $('form').submit((e) => {

            e.preventDefault();
            var data = this.getData();
            this.sendData(data)

        })

    }

    getData(){
        var name = $('#inputName').val();
        var email = $('#inputEmail').val();
        var info = $('#inputInfo').val();

        return {name : name, email : email, info : info}
    }

    sendData(data){

        console.log(data)

        $.ajax({
            url: '/sendContactForm',
            type: 'post',
            data: data,
            success: (response) => {
                console.log(response.success)
                if (response.success){
                    this.view.createSuccessAlert('Thanks!')
                }else{
                    this.view.createErrorModal()
                    $('.modal').modal()
                }
            }
        })

    }


}

const app = new controllerContactForm(new viewContact());