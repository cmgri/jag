import { viewEntryGallery } from './viewEntryGallery.mjs'

export class controllerEntryGallery {

    constructor(view) {
        this.view = view;

        $('document').ready(() => {
            this.getActiveCollections((collections) => {
                this.view.loadCollections(collections);
                this.bindListeners()
            });

        });

    }

    getActiveCollections(callback) {
        $.get('/getActiveCollections', (data, status) => {

            console.log(data)
            callback(data);
        });
    };

    bindListeners(){
        $('h5 a').click((e)=>{
            e.preventDefault()
            var colId = $(e.target).attr('id')
            window.localStorage.setItem('colId', colId)
            window.location = './showroom.html'
        })
    }




}

const app = new controllerEntryGallery(new viewEntryGallery());
