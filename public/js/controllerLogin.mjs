import {viewLogin} from './viewLogin.mjs'

export class controllerLogin {

    constructor(viewLogin) {

        this.view = viewLogin

        this.login = "";
        this.href = "";

        $('form').submit((e) => {

            e.preventDefault();

            if ($("input[name='userType']:checked").val() == 'User') {
                this.login = '/loginAuthor'
                this.href = '/uploadCollection.html'
            } else {
                this.login = '/loginAdmin'
                this.href = '/adminPanel.html'
            }
            var credentials = this.getCredentials();

            this.sendCredentials(credentials)

        })

    }

    getCredentials() {

        var email = $('#inputEmail').val();
        var password = $('#inputPassword').val();

        return { email: email, password: password }
    }

    sendCredentials(credentials) {

        $.ajax({
            url: this.login,
            type: 'post',
            data: credentials,
            success: (response) => {
                if (!response.success) {
                    this.view.createErrorAlert()
                    $('.alert-danger').text("Please check your password and email. And don't forget to select the checkbox!")
                } else {
                    sessionStorage.setItem("id", response.success);
                    window.location.href = this.href;
                }
            }
        })

    }
}

const app = new controllerLogin(new viewLogin());