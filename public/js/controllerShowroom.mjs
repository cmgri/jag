import { viewShowroom } from './viewShowroom.mjs'

export class controllerShowroom {

    constructor(view) {
        this.view = view;
        this.collection = {}
        this.author = {}
        this.id_col = window.localStorage.getItem('colId')

        this.getCollection()

        $('#carouselControls').on('slid.bs.carousel', () => {

            var e = $('.active').attr('id')
            var id = e.substr(e.length - 1, 1)

            console.log(id)

            $('#artwTitle').text(this.collection.artworks[id].name)
            $('#artwDesc').text(this.collection.artworks[id].desc)
        })
    }

    getCollection() {

        $.ajax({
            url: '/getCollection/' + this.id_col,
            type: 'get',
            success: (response) => {
                if (!response) {
                    console.log('Error')
                } else {
                    console.log(response)
                    this.collection = response;
                    this.populateCollection()
                    $.ajax({
                        url: '/getAuthor/' + this.collection.id_aut,
                        type: 'get',
                        success: (response) => {
                            if (!response.success) {
                                console.log('Error')
                            } else {
                                this.author = response.success;
                                this.populateAuthor()
                            }
                        }
                    })
                }
            }
        })

        $.ajax({
            url: '/getArtworks/' + this.id_col,
            type: 'get',
            success: (response) => {
                if (!response) {
                    console.log('Error')
                } else {

                    this.collection.artworks = response;
                    this.populateCarousel()
                }
            }
        })
    }

    populateAuthor() {
        $('.rowAuthor h3').text(this.author.name + ' ' + this.author.surname)
        $('.rowAuthor p').text(this.author.desc)
        $('.img-author').attr('src', this.author.img)
    }

    populateCollection(){
        $('.rowCollection h3').text(this.collection.name)
        $('.rowCollection p').text(this.collection.desc)
    }

    populateCarousel() {

        var i = 0

        this.view.createCarouselInner(this.collection.artworks.length)

        this.collection.artworks.forEach(artwork => {

            $('#itemImg' + i).attr('src', artwork.img)

            if (i == 0) {
                $('#artwTitle').text(artwork.name)
                $('#artwDesc').text(artwork.desc)
            }
            i++
        });
    }

}

const app = new controllerShowroom(new viewShowroom());
