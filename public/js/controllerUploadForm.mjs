import { viewUploadForm } from './viewUploadForm.mjs'

export class controllerUploadForm {

    constructor(view) {

        this.view = view;
        this.author = {};
        this.collection = null;
        this.artworks = [];
        this.hasCol = false;
        this.rowNum = 0;
        this.id_aut = "";


        if (document.title !== 'AdminPanel') {
            $('document').ready(() => {

                this.id_aut = sessionStorage.getItem("id");

                this.getAuthor(this.id_aut, () => {
                    this.hasCollection(this.id_aut, (result) => {
                        this.hasCol = result
                        if (this.hasCol) {
                            this.fillArtworks();
                        }
                    });

                    this.populateFormAuthor();

                });


            })

            $('form').submit((e) => {

                e.preventDefault();
                this.saveArtworks();
                this.sendData();
            })

            this.bindNextBtnListener();
            this.bindArtwListeners();
        }

    }

    hasCollection(id_aut, callback) {
        $.ajax({
            url: '/hasCollection/' + id_aut,
            type: 'get',
            success: (response) => {
                if (!response.success) {
                    callback(false);

                } else {
                    this.collection = response.success;
                    this.collection.imgName = this.collection.img
                    console.log(this.collection)
                    callback(true);

                }
            }
        })
    }

    fillArtworks() {

        if (this.collection.artworks.length > 0) {
            this.collection.artworks.forEach(artwork => {

                artwork.imgName = artwork.img;
                this.artworks.push(artwork);

            })
        }
        delete this.collection.artworks

    }

    getAuthor(id_aut, callback) {

        $.ajax({
            url: '/getAuthor/' + id_aut,
            type: 'get',
            success: (response) => {
                if (!response.success) {
                    this.view.createErrorModal()
                    $('.modal').modal()
                } else {
                    this.author = response.success;
                    callback()
                }
            }
        })

    }

    populateFormAuthor() {

        this.view.resetForm();

        this.view.createFormAuthor()

        $('#inputName').val(this.author.name);
        $('#inputSurname').val(this.author.surname);
        $('#inputDesc').val(this.author.desc);
        $('#img').attr('src', this.author.img);
        $('#labelFileAuthor').text(this.author.img);

        this.bindFileInput()

    }

    populateFormCollection() {

        this.view.resetForm();
        this.view.createFormCollection();

        $('#inputName').val(this.collection.name);
        $('#inputDesc').val(this.collection.desc);
        $('#img').attr('src', this.collection.img);
        $('#labelFileCollection').text(this.collection.img);

        console.log(this.collection)

        this.bindBackBtnListener();
        this.bindFileInput()

    }

    populateFormArtwork(addRow = false) {

        this.view.resetForm();

        this.artworks.forEach(artwork => {

            this.view.createFormArtwork();

            $('#submit-button').click(() => {
                this.sendData();
            })

            if (this.rowNum < 1) {

                $('#inputName').val(artwork.name);
                $('#inputDesc').val(artwork.desc);
                $('#labelFileArtwork').text(artwork.img);
                $('#img').attr('src', artwork.img);

            } else {

                this.view.addRowArtworks();

                $('#inputName' + this.rowNum).val(artwork.name);
                $('#inputDesc' + this.rowNum).val(artwork.desc);
                $('#labelFile' + this.rowNum).text(artwork.img);
                $('#img' + this.rowNum).attr('src', artwork.img);

                this.bindArtwListeners(this.rowNum);
            }

            this.bindFileInput()

            this.rowNum++

        })

    }

    bindArtwListeners(id = "") {

        $('#add-button').unbind('click');

        $('#add-button').click((e) => {
            id = this.view.addRowArtworks();
            this.rowNum = id
            this.bindArtwListeners(id);
        })

        $('#delete-button' + id).click((e) => {
            if ($(e.target).attr('id') == 'delete-button') {
                var formContainer = e.target.parentElement.parentElement.parentElement;
                this.removeArtwork(formContainer)
                this.view.resetForm();
            } else {
                this.rowNum--
                if (this.rowNum <= 0) {
                    this.rowNum = ""
                }
                this.view.moveAddBtn($("#addCol"))

                var formContainer = e.target.parentElement.parentElement.parentElement;
                this.removeArtwork(formContainer)
                formContainer.remove()
                this.view.moveAddBtn($("div[id^=addCol]").last())
            }

        })
    }

    bindNextBtnListener() {

        $('#next-btn').unbind('click');

        $('#next-btn').click(() => {
            var elmnt = document.getElementById("navbarNav");
            elmnt.scrollIntoView();

            if ($('#next-btn').hasClass('col-btn')) {

                this.saveAuthor();

                if (this.collection !== null) {
                    this.populateFormCollection();
                } else {
                    this.view.resetForm();
                    this.bindNextBtnListener();
                    this.bindBackBtnListener();
                    this.view.createFormCollection();
                }
            } else {

                if ($('#next-btn').hasClass('submit-btn')) {
                    $('form').trigger('submit');
                } else {

                    this.saveCollection();
                    if (this.artworks.length !== 0) {
                        this.populateFormArtwork();
                    } else {
                        this.view.resetForm();
                        this.view.createFormArtwork();
                        this.bindBackBtnListener();
                        this.bindArtwListeners();
                    }
                }

            }
        })

    }

    bindBackBtnListener() {

        $('#back-btn').unbind('click');
        $('#back-btn').click(() => {

            var elmnt = document.getElementById("navbarNav");
            elmnt.scrollIntoView();

            if ($('#back-btn').hasClass('col-btn')) {
                this.saveArtworks();
                this.rowNum = 0;
                this.view.removeAllRows();
                this.view.resetForm();
                this.bindNextBtnListener();
                this.bindBackBtnListener();

                if (this.collection) {
                    this.populateFormCollection();
                } else {
                    this.view.createFormCollection();
                }
            } else {
                this.saveCollection()
                this.populateFormAuthor()
            }

        })
    }

    bindFileInput() {
        $('.custom-file-input').unbind('change')
        $('.custom-file-input').change((e) => {
            var file = $(e.target)[0].files[0].name;
            $(e.target).next('label').text(file);
        })
    }

    saveAuthor() {

        var newValues = this.getData('Author')

        this.author.name = newValues.name;
        this.author.surname = newValues.surname;
        this.author.desc = newValues.desc;

        if (newValues.img !== undefined) {
            this.author.img = newValues.img;
        }

    }

    saveCollection() {

        var newValues = this.getData('Collection')

        if (this.collection == undefined) {
            this.collection = {}
        }

        this.collection.name = newValues.name;
        this.collection.desc = newValues.desc;

        if (newValues.img !== undefined) {
            this.collection.img = newValues.img;
            this.collection.imgName = newValues.img.name;
        } else {
            var imgName = this.collection.img.split("/");
            this.collection.imgName = imgName[imgName.length - 1]
        }

    }

    saveArtworks() {

        var rows = $('.form-container');
        var img = undefined

        if ($('#inputName').val() !== undefined && $('#inputName').val() !== "") {

            rows.each((i) => {

                var id = '#' + $(rows[i]).attr('id');

                var name = $(id + ' input[id^=inputName]').val();
                var desc = $(id + ' textarea[id^=inputDesc]').val();

                if (i == 0) {
                    img = document.querySelector(id + ' #divFileArtwork input[id^=customFile]').files[0];
                } else {
                    img = document.querySelector(id + ' input[id^=customFile]').files[0];
                }

                if (this.artworks[i] == undefined) {
                    this.artworks.push({})
                    this.artworks[i].id = 0;
                    if (this.collection.id !== undefined) {
                        this.artworks[i].id_col = this.collection.id;
                    } else {
                        this.artworks[i].id_col = 0
                    }

                }

                this.artworks[i].name = name;
                this.artworks[i].desc = desc;

                if (img !== undefined) {
                    this.artworks[i].img = img;
                    this.artworks[i].control = "0";
                    this.artworks[i].imgName = img.name;
                } else {
                    var imgName = this.artworks[i].img.split("/");
                    this.artworks[i].imgName = imgName[imgName.length - 1]
                }
            })
        }
    }

    getData(id) {

        var name = $('#inputName').val();
        var desc = $('#inputDesc').val();
        var img = document.getElementById('customFile' + id).files[0];

        if ($('#inputSurname').is(':visible')) {
            var surname = $('#inputSurname').val();
            return { name: name, surname: surname, desc: desc, img: img }
        }

        return { name: name, desc: desc, img: img }
    }

    removeArtwork(formContainer) {

        var containerid = formContainer.getAttribute('id')
        var name = $('#' + containerid + ' input[id^=inputName]').val()

        var i = 0;
        var toDelete = "";

        this.artworks.forEach(artwork => {
            if (artwork.name == name) {
                toDelete = i;
            }
            i++
        })

        delete this.artworks[toDelete];
    }

    sendData() {

        var formDataAuthor = new FormData();

        for (let [key, value] of Object.entries(this.author)) {
            formDataAuthor.append(key, value)
        }

        var formDataCollection = new FormData();

        for (let [key, value] of Object.entries(this.collection)) {
            formDataCollection.append(key, value)
        }

        var formDataArtworks = new FormData();

        this.artworks.forEach(artwork => {
            for (let [key, value] of Object.entries(artwork)) {
                formDataArtworks.append(key, value)
            }
        })

        $.ajax({
            url: '/setAuthor/' + this.id_aut,
            type: 'POST',
            data: formDataAuthor,
            processData: false,
            contentType: false,
            success: (response) => {
                if (response.success) {
                    this.view.createSuccessAlert('Submited!')
                } else {
                    this.view.createErrorModal()
                    $('.modal').modal()
                }
            }
        });

        if (this.hasCol) {

            console.log(this.collection)

            $.ajax({
                url: '/updatePendentCollection/' + this.id_aut,
                type: 'POST',
                data: formDataCollection,
                processData: false,
                contentType: false,
                success: (response) => {
                    if (response.success) {

                    } else {
                        this.view.createErrorModal()
                        $('.modal').modal()
                    }
                }
            });

            $.ajax({
                url: '/updatePendentArtworks/' + this.id_aut,
                type: 'POST',
                data: formDataArtworks,
                processData: false,
                contentType: false,
                success: (response) => {
                    if (response.success) {
                        this.view.createSuccessAlert('Submited!')
                    } else {
                        this.view.createErrorModal()
                        $('.modal').modal()
                    }
                }
            });
        } else {
            $.ajax({
                url: '/setPendentCollection/' + this.id_aut,
                type: 'post',
                data: formDataCollection,
                processData: false,
                contentType: false,
                success: (response) => {
                    if (response.success) {
                        $.ajax({
                            url: '/setPendentArtwork/' + this.id_aut,
                            type: 'POST',
                            data: formDataArtworks,
                            processData: false,
                            contentType: false,
                            success: (response) => {
                                if (response.success) {
                                    this.view.createSuccessAlert('Submited!')
                                    $('#main').height('100%')
                                } else {
                                    this.view.createErrorModal()
                                    $('.modal').modal()
                                }
                            }
                        });
                    } else {
                        this.view.createErrorModal()
                        $('.modal').modal()
                    }
                }
            });


        }



    }

}

const app = new controllerUploadForm(new viewUploadForm());