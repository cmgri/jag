

export class View {
    constructor() {

        
    }

    createMenu(container) {

        var navbar = this.createElement('nav', ['navbar', 'navbar-expand-lg']);
        var btnToggler = this.createElement('button', ['navbar-toggler'], [['type', 'button'], ['data-toggle', 'collapse'], ['data-target', '#navbarNav'], ['aria-controls', 'navbarNav'], ['aria-expanded', 'false'], ['aria-label', 'Toggle Navidation']]);
        var icon = this.createElement('i', ['fa', 'fa-bars'])
        var span = this.createElement('span', ['navbar-toggler-icon'])
        var navBarNav = this.createElement('div', ['collapse', 'navbar-collapse', 'justify-content-center', 'text-center'], [['id', 'navbarNav']])
        var ul = this.createElement('ul', ['navbar-nav'])
        var itemHome = this.createElement('li', ['nav-item', 'mr-5'])
        var linkHome = this.createElement('a', ['nav-link'], [['href', './index.html']])
        linkHome.html('Home')
        var itemCol = this.createElement('li', ['nav-item', 'ml-5', 'mr-5'])
        var linkCol = this.createElement('a', ['nav-link'], [['href', './collections.html']])
        linkCol.html('Collections')
        var itemNews = this.createElement('li', ['nav-item', 'ml-5', 'mr-5'])
        var linkNews = this.createElement('a', ['nav-link'], [['href', './newsletter.html']])
        linkNews.html('Newsletter')
        var itemCont = this.createElement('li', ['nav-item', 'ml-5', 'mr-5'])
        var linkCont = this.createElement('a', ['nav-link'], [['href', './contact.html']])
        linkCont.html('Contact')
        var itemLogin = this.createElement('li', ['nav-item', 'ml-5'])
        var linkLogin = this.createElement('a', ['nav-link'], [['href', './login.html']])
        linkLogin.html('Login')

        $(navbar).insertBefore(container)
        $(navbar).append(btnToggler)
        $(btnToggler).append(span)
        $(span).append(icon)
        $(navbar).append(navBarNav)
        $(navBarNav).append(ul)
        $(ul).append(itemHome)
        $(itemHome).append(linkHome)
        $(ul).append(itemCol)
        $(itemCol).append(linkCol)
        $(ul).append(itemNews)
        $(itemNews).append(linkNews)
        $(ul).append(itemCont)
        $(itemCont).append(linkCont)
        $(ul).append(itemLogin)
        $(itemLogin).append(linkLogin)
    }

    createElement(tag, classes = [], attrs = []) {
        const element = $('<' + tag + '></' + tag + '>');
        if (classes) {
            classes.forEach(className => {
                element.addClass(className)
            })
        }
        if (attrs) {

            attrs.forEach(attr => {
                element.attr(attr[0], attr[1]);
            })


        }
        return element;


    }

    createAddAuthorForm(panel) {

        $('#rowNew').remove()

        var form = this.createElement('form', ['author-form', 'text-dark'])
        var container = this.createElement('div', ['form-container'], [['id', 'form-container']]);

        var title = this.createElement('h4')
        title.text('New Author')

        var rowEmail = this.createElement('div', ['row'])

        var colEmail = this.createElement('div', ['col'])
        var formGroupEmail = this.createElement('div', ['form-group']);
        var labelEmail = this.createElement('label', [''], [['for', 'inputEmail'], ['id', 'labelEmail']]);
        $(labelEmail).text('Email')
        var inputEmail = this.createElement('input', ['form-control'], [['type', 'text'], ['id', 'inputEmail']]);

        var colPaswd = this.createElement('div', ['col'], [['id', 'colPaswd']])
        var formGroupPaswd = this.createElement('div', ['form-group']);
        var labelPaswd = this.createElement('label', [''], [['for', 'inputPaswd'], ['id', 'labelPaswd']]);
        $(labelPaswd).text('Password')
        var inputPaswd = this.createElement('input', ['form-control'], [['type', 'text'], ['id', 'inputPaswd']]);
        var rowSend = this.createElement('div', ['row', 'justify-content-center', 'p-3'])
        var sendBtn = this.createElement('button', ['btn', 'btn-outline-info'], [['id', 'btnSend'], ['type', 'button']])
        sendBtn.text('Send')

        $(panel).append(form)
        $('form').append(container);
        container.append(title)
        container.append(rowEmail);
        rowEmail.append(colEmail);

        colEmail.append(formGroupEmail);
        formGroupEmail.append(labelEmail);
        formGroupEmail.append(inputEmail);

        rowEmail.append(colPaswd);
        colPaswd.append(formGroupPaswd);
        formGroupPaswd.append(labelPaswd);
        formGroupPaswd.append(inputPaswd);
        $(panel).append(rowSend)
        $(rowSend).append(sendBtn)
    }

    createForm(panel, id = "", author = false) {

        if (!author) {

            this.createTabs(panel)
        }

        var form = this.createElement('form')
        $(panel).append(form)

        this.createInnerForm()

        var divBtn = this.createElement('div', ['row', 'justify-content-center', 'p-3'], [['id', 'divSave']])
        var saveBtn = this.createElement('button', ['btn', 'btn-outline-info'], [['id', 'btnSave'], ['type', 'button']])
        saveBtn.text('Save')
        $(panel).append(divBtn)
        $(divBtn).append(saveBtn)

    }

    createInnerForm(id = "", artwork = false) {

        var container = this.createElement('div', ['form-container', 'text-dark'], [['id', 'form-container' + id]]);

        var rowName = this.createElement('div', ['row'])

        var colName = this.createElement('div', ['col'])
        var formGroupName = this.createElement('div', ['form-group']);
        var labelName = this.createElement('label', [''], [['for', 'inputName' + id], ['id', 'labelName' + id]]);
        $(labelName).text('Name')
        var inputName = this.createElement('input', ['form-control'], [['type', 'text'], ['id', 'inputName' + id]]);

        var colSurname = this.createElement('div', ['col'], [['id', 'surnameCol'], ['style', 'display: none;']])
        var formGroupSurname = this.createElement('div', ['form-group']);
        var labelSurname = this.createElement('label', [''], [['for', 'inputSurname' + id], ['id', 'labelSurname' + id]]);
        $(labelSurname).text('Surname')
        var inputSurname = this.createElement('input', ['form-control'], [['type', 'text'], ['id', 'inputSurname' + id]]);

        var formGroupDesc = this.createElement('div', ['form-group']);
        var labelDesc = this.createElement('label', [''], [['for', 'inputDesc' + id], ['id', 'labelDesc' + id]]);
        $(labelDesc).text('Description')
        var textArea = this.createElement('textarea', ['form-control'], [['id', 'inputDesc' + id], ['rows', 3]]);

        var labelImg = this.createElement('label', [''], [['for', 'customFile' + id], ['id', 'labelImg' + id]]);
        $(labelImg).text('Image');
        var divfile = this.createElement('div', ['custom-file']);
        var inputFile = this.createElement('input', ['custom-file-input'], [['type', 'file'], ['id', 'customFile' + id]]);
        var labelFile = this.createElement('label', ['custom-file-label'], [['for', 'customFile' + id], ['id', 'labelFile' + id]]);
        var img = this.createElement('img', ['w-25'], [['id', 'img' + id], ['style', 'min-width: 100px; min-height: 140px;']])

        $('form').append(container);
        container.append(rowName);
        rowName.append(colName);

        colName.append(formGroupName);
        formGroupName.append(labelName);
        formGroupName.append(inputName);

        rowName.append(colSurname);
        colSurname.append(formGroupSurname);
        formGroupSurname.append(labelSurname);
        formGroupSurname.append(inputSurname);

        container.append(formGroupDesc);
        formGroupDesc.append(labelDesc);
        formGroupDesc.append(textArea);

        container.append(labelImg);
        container.append(divfile);
        divfile.append(inputFile);
        divfile.append(labelFile);
        divfile.append(img);

        this.createArtworkBtns(id, container)

        if (artwork) {
            $('#artwrow').show()
            container.addClass('addContainer')

        } else {
            $('#artwrow').hide()
        }

    }

    createTabs(panel) {

        var navTabs = this.createElement('ul', ['nav', 'nav-tabs'], [['id', 'navTabs']])
        var itemCol = this.createElement('li', ['nav-item'], [['id', 'itemCol']])
        var linkCol = this.createElement('a', ['nav-link', 'active'], [['id', 'linkCol'], ['href', '#']])
        linkCol.text('Collection')
        var itemArtw = this.createElement('li', ['nav-item'], [['id', 'itemArtw']])
        var linkArtw = this.createElement('a', ['nav-link'], [['id', 'linkArtw'], ['href', '#']])
        linkArtw.text('Artworks')

        panel.append(navTabs)

        navTabs.append(itemCol)
        itemCol.append(linkCol)

        navTabs.append(itemArtw)
        itemArtw.append(linkArtw)
    }

    createRowNew() {
        var rowNew = this.createElement('div', ['row', 'justify-content-center'], [['id', 'rowNew']])
        var btnNew = this.createElement('button', ['btn', 'btn-outline-info', 'm-2'], [['id', 'btnNew']])
        btnNew.text('Add new')

        rowNew.insertBefore($('table'))
        rowNew.append(btnNew)
    }

    createArtworkBtns(id, formContainer) {

        var artwRow = this.createElement('div', ['row'], [['id', 'artwrow' + id]]);

        var delCol = this.createElement('div', ['col', 'del-col'])
        var delBtn = this.createElement('button', ['btn', 'btn-danger', 'btn-del'], [['id', 'delete-button' + id], ['type', 'button']])
        delBtn.text('Remove')

        var addCol = this.createElement('div', ['col', 'del-col'], [['id', 'addCol' + id]])

        $(formContainer).append(artwRow)
        artwRow.append(delCol)
        delCol.append(delBtn)
        artwRow.append(addCol)

        var addButton = $('#add-button')

        if (addButton.length !== 0) {
            addCol.append(addButton)

        } else {
            var addBtn = this.createElement('button', ['btn', 'btn-success', 'btn-add'], [['id', 'add-button' + id], ['type', 'button']])
            addBtn.text('Add')
            addCol.append(addBtn)
        }

    }

    createErrorAlert(text = ""){
        
        $('.alert-danger').remove()
        var error = this.createElement('div', ['alert', 'alert-danger', 'mt-2'], [['role', 'alert']])
        error.text(text)
        $('form').append(error)

    }

    createSuccessAlert(text = ""){

        var success = this.createElement('div', ['alert', 'alert-info', 'mt-2'], [['role', 'alert']])
        success.text(text)
        $('form').empty()
        $('form').append(success)

    }

    createErrorModal(){

        $('.modal').remove()

        var modal = this.createElement('div', ['modal'], [['tabindex', '-1'], ['role', 'dialog']])
        var modalDial = this.createElement('div', ['modal-dialog'], [['role', 'document']])
        var modalCont = this.createElement('div', ['modal-content'])
        var modalHead = this.createElement('div', ['modal-header'])
        var modalTitle = this.createElement('h5', ['modal-title'])
        modalTitle.text('Oops!')
        var btnClose = this.createElement('button', ['close'], [['type', 'button'], ['data-dismiss', 'modal'], ['aria-label', 'close']])
        var span = this.createElement('span',[], [['aria-hidden', 'true']])
        span.html('&times;')
        var modalBody = this.createElement('div', ['modal-body'])
        var modalText = this.createElement('p')
        modalText.text('Something went wrong. Please refresh and try again!')
        var modalFooter = this.createElement('div', ['modal-footer'])
        var btnFotClose = this.createElement('button', ['btn'], [['type', 'button'], ['data-dismiss', 'modal']])

        $('body').append(modal)
        modal.append(modalDial)
        modalDial.append(modalCont)
        modalCont.append(modalHead)
        modalHead.append(modalTitle)
        modalHead.append(btnClose)
        btnClose.append(span)
        modalCont.append(modalBody)
        modalBody.append(modalText)
        modalCont.append(modalFooter)
        modalFooter.append(btnFotClose)
    }

    getElement(selector) {

        var element = $(selector);
        if (element.length == 0) { element = null }
        return element;
    }

}