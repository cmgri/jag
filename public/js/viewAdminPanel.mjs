import { View } from './view.mjs'

export class viewAdminPanel extends View {

    constructor(){

        super();

        this.createMenu($('#container'))
    }

    fillTableHead(columns) {

        for (var i = 0; i <= columns; i++) {

            var th = this.createElement('th', [], [['scope', 'col']])

            $('thead > tr').append(th)
        }
    }

    createPanelActiveCollections(rows) {

        this.resetTable()
        this.showTable()
        this.fillTableHead(4)

        for (var i = 0; i < rows; i++) {

            var tr = this.createElement('tr', [], [['id', 'tr' + i]])
            var th = this.createElement('th', [], [['scope', 'row']])
            th.text(i + 1);
            var tdName = this.createElement('td', [], [['id', 'tdName' + i]])
            var tdAuthor = this.createElement('td', [], [['id', 'tdAuthor' + i]])
            var tdImg = this.createElement('td', [], [['id', 'tdImg' + i]])
            var img = this.createElement('img', ['w-25'], [['id', 'img' + i]])
            var tdBtn = this.createElement('td', [], [['id', 'tdBtn' + i]])
            var btnDetail = this.createElement('button', ['btn', 'btn-detail'], [['id', 'btnDetail' + i]])
            btnDetail.text('Details')

            $('tbody').append(tr)
            tr.append(th)
            tr.append(tdName)
            tr.append(tdAuthor)
            tr.append(tdImg)
            tdImg.append(img)
            tr.append(tdBtn)
            tdBtn.append(btnDetail)
        }
    }

    createPanelPendentCollections(rows) {

        this.resetTable()
        this.showTable()
        this.fillTableHead(6)

        for (var i = 0; i < rows; i++) {

            var tr = this.createElement('tr', [], [['id', 'tr' + i]])
            var th = this.createElement('th', [], [['scope', 'row']])
            th.text(i + 1);
            var tdName = this.createElement('td', [], [['id', 'tdName' + i]])
            var tdAuthor = this.createElement('td', [], [['id', 'tdAuthor' + i]])
            var tdDate = this.createElement('td', [], [['id', 'tdDate' + i]])
            var tdImg = this.createElement('td', [], [['id', 'tdImg' + i]])
            var img = this.createElement('img', ['w-25'], [['id', 'img' + i]])
            var tdBtn = this.createElement('td', [], [['id', 'tdBtn' + i]])
            var btnDetail = this.createElement('button', ['btn', 'btn-pendent'], [['id', 'btnDetail' + i]])
            btnDetail.text('Details')
            var tdBtnVal = this.createElement('td', [], [['id', 'tdBtnVal' + i]])
            var btnValidate = this.createElement('button', ['btn', 'btn-outline-info'], [['id', 'btnValidate' + i]])
            btnValidate.text('Validate')


            $('tbody').append(tr)
            tr.append(th)
            tr.append(tdName)
            tr.append(tdAuthor)
            tr.append(tdDate)
            tr.append(tdImg)
            tdImg.append(img)
            tr.append(tdBtn)
            tdBtn.append(btnDetail)
            tr.append(tdBtnVal)
            tdBtnVal.append(btnValidate)

        }
    }

    createPanelAuthorsManager(rows) {

        this.resetTable()
        this.showTable()
        this.createRowNew()
        this.fillTableHead(4)

        for (var i = 0; i < rows; i++) {

            var tr = this.createElement('tr', [], [['id', 'tr' + i]])
            var th = this.createElement('th', [], [['scope', 'row']])
            th.text(i + 1);
            var tdName = this.createElement('td', [], [['id', 'tdName' + i]])
            var tdEmail = this.createElement('td', [], [['id', 'tdEmail' + i]])
            var tdImg = this.createElement('td', [], [['id', 'tdImg' + i]])
            var img = this.createElement('img', ['w-25'], [['id', 'img' + i]])
            var tdBtn = this.createElement('td', [], [['id', 'tdBtn' + i]])
            var btnDetail = this.createElement('button', ['btn', 'btn-detail'], [['id', 'btnAuthorDetail' + i]])
            btnDetail.text('Details')

            $('tbody').append(tr)
            tr.append(th)
            tr.append(tdName)
            tr.append(tdEmail)
            tr.append(tdImg)
            tdImg.append(img)
            tr.append(tdBtn)
            tdBtn.append(btnDetail)

        }
    }

    createPanelOldCollections(rows) {

        this.resetTable()
        this.showTable()
        this.fillTableHead(4)

        for (var i = 0; i < rows; i++) {

            var tr = this.createElement('tr', [], [['id', 'tr' + i]])
            var th = this.createElement('th', [], [['scope', 'row']])
            th.text(i+1);
            var tdName = this.createElement('td', [], [['id', 'tdName' + i]])
            var tdAuthor = this.createElement('td', [], [['id', 'tdAuthor' + i]])
            var tdImg = this.createElement('td', [], [['id', 'tdImg' + i]])
            var img = this.createElement('img', ['w-25'], [['id', 'img' + i]])
            var tdBtn = this.createElement('td', [], [['id', 'tdBtn' + i]])
            var btnDetail = this.createElement('button', ['btn', 'btn-old'], [['id', 'btnDetail' + i]])
            btnDetail.text('Details')

            $('tbody').append(tr)
            tr.append(th)
            tr.append(tdName)
            tr.append(tdAuthor)
            tr.append(tdImg)
            tdImg.append(img)
            tr.append(tdBtn)
            tdBtn.append(btnDetail)

        }
    }

    createPanelEditCollection() {

        this.hideTable()
        this.createForm($('#contentPanel'))

    }

    createPanelEditArtworks(rows) {

        this.removeAllRows()
        for (var i = 0; i < rows - 1; i++) {
            this.createInnerForm(i, true)
        }
    }

    createPanelAddAuthor(){
        this.hideTable()
        this.createAddAuthorForm($('#contentPanel'))
    }

    createPanelEditAuthor() {

        this.hideTable()
        $('rowNew').remove()
        this.createForm($('#contentPanel'), "", true)
        $('#surnameCol').show()

    }

    disableInputs() {
        $('#inputName').attr('disabled', 'disabled')
        $('#inputDesc').attr('disabled', 'disabled')
        $('#customFile').attr('disabled', 'disabled')
    }

    resetTable() {

        $('thead > tr th').remove()
        $('tbody').empty()
        $('#btnNew').remove()
    }

    hideTable() {
        $('table').hide()
    }

    showTable() {
        $('#navTabs').remove()
        $('#divSave').remove()
        $('#btnSend').remove()
        $('#rowNew').remove()
        $('form').remove()
        $('table').show()
    }

    removeAllRows() {
        $('.addContainer').remove()
        $('#artwrow').hide()
    }

    removeRow(id) {
        $('#' + id).remove()
    }

    resetForm() {
        $('#inputName').val("")
        $('#inputDesc').val("")
        $('#input').val("")
        $('#img').attr('src', "");
        $('#labelFile').text("");
    }

    setSelected(item){
        $('.selectedItem').removeClass('selectedItem')
        $(item).addClass('selectedItem')
    }

    removeAuthorRows(){
        $('#rowNew').remove()
        $('btnSave').remove()
    }

}