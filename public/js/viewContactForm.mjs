import {View} from './view.mjs'

export class viewContact extends View{

    constructor(){

        super();

        this.createMenu($('#container'))

        $('a[href="./contact.html"]').addClass('selected')
        
    }
}