import {View} from './view.mjs'

export class viewEntryGallery extends View {

    constructor() {

        super();

        this.createMenu($('#container'))
        this.app = this.getElement('.container');

        $('a[href="./collections.html"]').addClass('selected')

    }

    loadCollections(collections) {

        var columns = 0;

        var row = this.createElement('div', ['row'])
        collections.forEach(collection => {

            if (columns % 3 == 0) {
                row = this.createElement('div', ['row', 'row-cols-sm-2', 'row-cols-md-3']);
                $('#collections').append(row);
            }

            var column = this.createElement('div', ['col-sm']);
            var card = this.createElement('div', ['div-collection', 'rounded-lg'])
            var img = this.createElement('img', ['card-img-top'], [['src', collection.img]]);
            var body = this.createElement('div');
            var title = this.createElement('h5', ['title']);
            var name = this.createElement('a',[], [['href', ''], ['id', collection.id]])
            name.text(collection.name)

            row.append(column);
            column.append(card);
            card.append(img);
            card.append(body);
            body.append(title);
            title.append(name);


            columns++;
        });

    }

}