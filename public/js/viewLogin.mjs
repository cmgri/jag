import {View} from './view.mjs'

export class viewLogin extends View{

    constructor(){

        super();

        this.createMenu($('#container'))

        $('a[href="./login.html"]').addClass('selected')
        
    }
}