import {View} from './view.mjs'

export class viewNewsletter extends View{

    constructor(){

        super();

        this.createMenu($('#container'))

        $('a[href="./newsletter.html"]').addClass('selected')
        
    }
}
