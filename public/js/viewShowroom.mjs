import { View } from './view.mjs'

export class viewShowroom extends View {

    constructor() {

        super();

        this.createMenu($('#container'))
        this.app = this.getElement('.container');

        $('a[href="./collections.html"]').addClass('selected')

    }

    createCarouselInner(items) {
    
        for(var i = 0; i < items; i++){

            var item = this.createElement('div', ['carousel-item'], [['id', 'item'+i]])
            var itemImg = this.createElement('img', ['w-75'], [['id', 'itemImg'+i]])

            $('.carousel-inner').append(item)
            item.append(itemImg)

            if(i == 0){
                item.addClass('active')
            }
        }   

    }

}