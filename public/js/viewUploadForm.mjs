import { View } from './view.mjs'

export class viewUploadForm extends View {

    constructor() {

        super();

        this.containerCount = 0;

        this.createMenu($('#container'))

    }

    createFormAuthor() {

        $('h3').text('About yourself')
        $('#surnameCol').show();
        $('#back-col').hide();
        $('#next-btn').addClass('col-btn')

        this.createImgInput('Author')

    }

    createFormCollection() {

        $('h3').text('About your collection')
        $('#surnameCol').hide();
        $('#artwrow').hide()
        $('#back-col').show();

        $('#next-btn').removeClass('col-btn');
        $('#back-btn').removeClass('col-btn');
        $('#next-btn').removeClass('submit-btn');

        $('#next-btn').text('Next');

        $('#back-btn').addClass('aut-btn');
        $('#next-btn').addClass('artw-btn');

        this.createImgInput('Collection')
    }

    createFormArtwork() {

        $('h3').text('About your artworks')

        $('#back-btn').removeClass('aut-btn');
        $('#back-btn').addClass('col-btn');

        $('#next-btn').removeClass('artw-btn');
        $('#next-btn').addClass('submit-btn');
        $('#next-btn').text('Submit');

        $('#artwrow').show()

        this.createImgInput('Artwork')
    }

    createImgInput(id) {
        
        if (this.getElement('div[id^=divFile]') !== null) {

            var divFile = this.getElement('div[id^=divFile]');
            $(divFile).hide()
        }

        if (this.getElement('#divFile' + id) == null) {

            var divFile = this.createElement('div', ['custom-file'], [['id', 'divFile' + id]]);
            var inputFile = this.createElement('input', ['custom-file-input'], [['type', 'file'], ['id', 'customFile' + id]]);
            var labelFile = this.createElement('label', ['custom-file-label'], [['for', 'customFile' + id], ['id', 'labelFile' + id]]);

            divFile.insertBefore('#img')
            divFile.append(inputFile);
            divFile.append(labelFile);

        }else{

            divFile = this.getElement('#divFile' + id);
            $(divFile).show()

        }

    }

    addRowArtworks() {

        this.containerCount++;

        var container = this.createElement('div', ['form-container', 'add-container'], [['id', 'form-container' + this.containerCount]]);

        var formGroupName = this.createElement('div', ['form-group']);
        var labelName = this.createElement('label', [''], [['for', 'inputName' + this.containerCount], ['id', 'labelName' + this.containerCount]]);
        $(labelName).text('Name')
        var inputName = this.createElement('input', ['form-control'], [['type', 'text'], ['id', 'inputName' + this.containerCount]]);

        var formGroupDesc = this.createElement('div', ['form-group']);
        var labelDesc = this.createElement('label', [''], [['for', 'inputDesc' + this.containerCount], ['id', 'labelDesc' + this.containerCount]]);
        $(labelDesc).text('Description')
        var textArea = this.createElement('textarea', ['form-control'], [['id', 'inputDesc' + this.containerCount], ['rows', 3]]);

        var labelImg = this.createElement('label', [''], [['for', 'customFile' + this.containerCount], ['id', 'labelImg' + this.containerCount]]);
        $(labelImg).text('Image');
        var divfile = this.createElement('div', ['custom-file']);
        var inputFile = this.createElement('input', ['custom-file-input'], [['type', 'file'], ['id', 'customFile' + this.containerCount]]);
        var labelFile = this.createElement('label', ['custom-file-label'], [['for', 'customFile' + this.containerCount], ['id', 'labelFile' + this.containerCount]]);
        var img = this.createElement('img', ['w-25'], [['id', 'img'+ this.containerCount]])

        var artRow = this.createElement('div', ['row', 'artRow'], [['id', 'artw-row' + this.containerCount]])
        var artcolAdd = this.createElement('div', ['col'], [['id', 'addCol' + this.containerCount]])
        var artcolDel = this.createElement('div', ['col', 'colDel'])
        var delBut = this.createElement('button', ['btn','btn-outline-dark', 'btn-del'], [['id', 'delete-button' + this.containerCount], ['type', 'button']])
        $(delBut).text('Remove');

        $('</hr>').after(container);
        $(container).insertBefore('#nav-row');
        container.append(formGroupName);
        formGroupName.append(labelName);
        formGroupName.append(inputName);

        container.append(formGroupDesc);
        formGroupDesc.append(labelDesc);
        formGroupDesc.append(textArea);

        container.append(labelImg);
        container.append(divfile);
        divfile.append(inputFile);
        divfile.append(labelFile);
        divfile.append(img);

        container.append(artRow)
        artRow.append(artcolDel)
        artcolDel.append(delBut)
        artRow.append(artcolAdd)
        this.moveAddBtn(artcolAdd)

        return this.containerCount

    }

    moveAddBtn(col){
        col.append($('#add-button'))
    }

    removeRowArtworks(id) {
        console.log('entra')
        $('#' + id).remove()
    }

    removeAllRows() {
        $('.add-container').remove()
        this.containerCount = 0
    }

    resetForm() {
        $('#form-container input').val("");
        $('#form-container textarea').val("");
        $('#form-container .custom-file-label').text("");
        $('#form-container #img').attr('src', "");
    }

}